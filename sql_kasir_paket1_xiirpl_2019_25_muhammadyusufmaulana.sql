-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Okt 2019 pada 09.20
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sql_kasir_paket1_xiirpl_2019_25_muhammadyusufmaulana`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `tambahMenu` (IN `_id` INT(11), IN `_id_kategori` INT(11), IN `nama_menu` VARCHAR(100), IN `harga` VARCHAR(100), IN `status_menu` ENUM('TERSEDIA','TIDAK TERSEDIA'), IN `deskripsi` VARCHAR(255), IN `foto` VARCHAR(255))  NO SQL
BEGIN
INSERT INTO tb_menu VALUES (_id, _id_kategori, _nama_menu, _harga, _status_menu, _deskripsi, _foto);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `four_table`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `four_table` (
`id` int(11)
,`id_user` int(11)
,`nama_pelanggan` varchar(100)
,`no_meja` enum('01','02','03','04','05','06','07','08','09','10','11','12','13')
,`waktu_order` varchar(100)
,`catatan` text
,`status_order` varchar(20)
,`id_menu` int(11)
,`id_kategori` int(11)
,`nama_menu` varchar(100)
,`harga` varchar(100)
,`status_menu` enum('TERSEDIA','TIDAK TERSEDIA')
,`deskripsi` varchar(255)
,`foto` varchar(255)
,`id_detail_order` int(11)
,`jumlah` int(11)
,`sub_total` varchar(100)
,`total_bayar` varchar(100)
,`status_detail_order` varchar(30)
,`id_transaksi` int(11)
,`dibayar` varchar(100)
,`history` varchar(10)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_detail_order`
--

CREATE TABLE `tb_detail_order` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `sub_total` varchar(100) NOT NULL,
  `total_bayar` varchar(100) NOT NULL,
  `status_detail_order` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_detail_order`
--

INSERT INTO `tb_detail_order` (`id`, `id_menu`, `id_order`, `jumlah`, `sub_total`, `total_bayar`, `status_detail_order`) VALUES
(31, 6, 47, 100, '13000000', '13.065.000', 'Telah Dibuat'),
(32, 14, 47, 100, '65000', '13.065.000', 'Telah Dibuat'),
(33, 6, 48, 100, '13000000', '13.000.000', 'Telah Dibuat'),
(34, 6, 49, 100, '13000000', '13.000.000', 'Telah Dibuat'),
(35, 6, 50, 10, '1300000', '1.306.680', 'Telah Dibuat'),
(36, 14, 50, 10, '6500', '1.306.680', 'Telah Dibuat'),
(37, 11, 50, 10, '180', '1.306.680', 'Telah Dibuat'),
(39, 6, 52, 1, '130000', '130.000', 'Telah Dibuat'),
(40, 6, 53, 1, '130000', '130.010', 'Telah Dibuat'),
(41, 8, 53, 1, '10', '130.010', 'Telah Dibuat'),
(42, 6, 54, 10, '1300000', '1.300.100', 'Telah Dibuat'),
(43, 8, 54, 10, '100', '1.300.100', 'Telah Dibuat'),
(44, 6, 55, 1, '130000', '130.000', 'Telah Dibuat'),
(45, 6, 56, 1, '130000', '130.010', 'Telah Dibuat'),
(46, 8, 56, 1, '10', '130.010', 'Telah Dibuat'),
(47, 14, 57, 1, '650', '650', 'Telah Dibuat'),
(48, 14, 58, 100, '65000', '66.800', 'Telah Dibuat'),
(49, 11, 58, 100, '1800', '66.800', 'Telah Dibuat'),
(50, 6, 59, 100, '13000000', '13.004.500', 'Telah Dipesan'),
(51, 10, 59, 100, '500', '13.004.500', 'Telah Dipesan'),
(52, 13, 59, 100, '4000', '13.004.500', 'Telah Dipesan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'MAKANAN'),
(2, 'MINUMAN'),
(3, 'PENCUCI MULUT'),
(4, 'PAKET HEMAT'),
(5, 'PAKET BESAR');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_level`
--

CREATE TABLE `tb_level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_level`
--

INSERT INTO `tb_level` (`id_level`, `nama_level`) VALUES
(1, 'ADMINISTRATOR'),
(2, 'OWNER'),
(3, 'KASIR'),
(4, 'WAITER'),
(5, 'PELANGGAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_menu` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `status_menu` enum('TERSEDIA','TIDAK TERSEDIA') NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id`, `id_kategori`, `nama_menu`, `harga`, `status_menu`, `deskripsi`, `foto`) VALUES
(6, 1, 'mi goreng', '130000', 'TERSEDIA', 'mie enak', 'uploads/3.jpg'),
(7, 2, 'ES SEGER', '8', 'TIDAK TERSEDIA', 'seger....', 'uploads/71.png'),
(8, 1, 'somay', '10', 'TERSEDIA', 'pedes', 'uploads/4.jpg'),
(10, 2, 'ES TAWAR', '5', 'TERSEDIA', 'HANGAT', 'uploads/12.png'),
(11, 2, 'JUS ALPUKET', '18', 'TERSEDIA', 'PURE NATURE', 'uploads/13.png'),
(12, 3, 'PUDING', '7000', 'TIDAK TERSEDIA', 'enak kenyal manis', 'uploads/14.png'),
(13, 4, 'MAM BURGER', '40', 'TERSEDIA', 'PAKET HEMAT', 'uploads/15.png'),
(14, 5, 'MAMAM BIG FAMILIY', '650', 'TERSEDIA', 'DIJAMIN PUAS', 'uploads/5.jpg'),
(15, 2, 'es jeruk', '15', 'TIDAK TERSEDIA', 'seger bat', 'uploads/151.png'),
(16, 1, 'AYAM BAKAR', '3500000', 'TERSEDIA', 'SEGER', 'uploads/6.jpg'),
(17, 1, 'AYAM GORENG', '500000', 'TERSEDIA', 'seger', 'uploads/27.png'),
(18, 1, 'AYAM YAYA', '5000000', 'TERSEDIA', 'RASANYA ENAK', 'uploads/18.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_order`
--

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `no_meja` enum('01','02','03','04','05','06','07','08','09','10','11','12','13') NOT NULL,
  `waktu_order` varchar(100) NOT NULL,
  `catatan` text,
  `status_order` varchar(20) NOT NULL,
  `status_pesanan` enum('SELESAI','DALAM PROSES') NOT NULL,
  `history` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_order`
--

INSERT INTO `tb_order` (`id`, `id_user`, `nama_pelanggan`, `no_meja`, `waktu_order`, `catatan`, `status_order`, `status_pesanan`, `history`) VALUES
(47, 27, 'Muhammad Yusuf Maulana', '03', '17/10/2019', 'Bismillah SOLV', 'Dibayar', 'SELESAI', 'SEMBUNYIKAN'),
(48, 24, 'waiter1', '01', '18/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'SEMBUNYIKAN'),
(49, 27, 'Muhammad Yusuf Maulana', '02', '18/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'SEMBUNYIKAN'),
(50, 27, 'Muhammad Yusuf Maulana', '04', '19/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'SEMBUNYIKAN'),
(52, 27, 'Muhammad Yusuf Maulana', '04', '19/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'SEMBUNYIKAN'),
(53, 27, 'Muhammad Yusuf Maulana', '05', '19/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'TAMPILKAN'),
(54, 27, 'Muhammad Yusuf Maulana', '02', '19/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'TAMPILKAN'),
(55, 24, 'waiter1', '02', '26/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'TAMPILKAN'),
(56, 27, 'Muhammad Yusuf Maulana', '05', '26/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'TAMPILKAN'),
(57, 24, 'waiter1', '03', '26/10/2019', 'Bismillah', 'Dibayar', 'SELESAI', 'TAMPILKAN'),
(58, 27, 'Muhammad Yusuf Maulana', '01', '27/10/2019', 'Bismillah', 'Dibayar', 'DALAM PROSES', 'TAMPILKAN'),
(59, 27, 'Muhammad Yusuf Maulana', '02', '27/10/2019', 'Bismillah', 'Belum Dibayar', 'DALAM PROSES', 'TAMPILKAN');

--
-- Trigger `tb_order`
--
DELIMITER $$
CREATE TRIGGER `updateStatus_Detail_Order` AFTER UPDATE ON `tb_order` FOR EACH ROW begin
update tb_detail_order
set status_detail_order = 'Telah Dibuat'
where id_order = new.id;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_detail_order` int(11) NOT NULL,
  `total_bayar` varchar(100) NOT NULL,
  `dibayar` varchar(100) NOT NULL,
  `history` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id`, `id_user`, `id_order`, `id_detail_order`, `total_bayar`, `dibayar`, `history`) VALUES
(3, 27, 47, 0, 'Rp. 13.065.000', 'Rp. 14.000.000', 'TAMPILKAN'),
(4, 27, 48, 0, 'Rp. 13.000.000', 'Rp. 15.000.000', 'TAMPILKAN'),
(5, 27, 49, 0, 'Rp. 13.000.000', 'Rp. 15.000.000', 'TAMPILKAN'),
(6, 27, 50, 0, 'Rp. 1.306.680', 'Rp. 15.000.000', 'TAMPILKAN'),
(8, 27, 54, 0, 'Rp. 1.300.100', 'Rp. 5.000.000', 'TAMPILKAN'),
(9, 22, 52, 0, 'Rp. 130.000', 'Rp. 500.000', 'TAMPILKAN'),
(10, 22, 53, 0, 'Rp. 130.010', 'Rp. 50.000', 'TAMPILKAN'),
(11, 22, 55, 0, 'Rp. 130.000', 'Rp. 150.000', 'TAMPILKAN'),
(12, 22, 56, 0, 'Rp. 130.010', 'Rp. 140.000', 'TAMPILKAN'),
(13, 22, 57, 0, 'Rp. 650', 'Rp. 1000', 'TAMPILKAN'),
(14, 27, 58, 0, 'Rp. 66.800', 'Rp. 70.000', 'TAMPILKAN');

--
-- Trigger `tb_transaksi`
--
DELIMITER $$
CREATE TRIGGER `ubahStatusOrder` AFTER INSERT ON `tb_transaksi` FOR EACH ROW begin
update tb_order
set status_order = 'Dibayar'
where id = new.id_order;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `id_level` int(5) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `id_level`, `foto`, `nama`, `email`, `pass`) VALUES
(22, 3, 'uploads/23.png', 'kasir1', 'kasir1@gmail.com', '$2y$10$oaXQsvtsEBqULHK0yj1mNetf/y8KI6FYeudFRyv.RZD/n4cPYy4He'),
(23, 2, 'uploads/24.png', 'owner1', 'owner1@gmail.com', '$2y$10$sa/rzNB6ky1u75THhV9Ajujrh8.NTppkL52gL2WAlhy.vTXy34p0.'),
(24, 4, 'uploads/25.png', 'waiter1', 'waiter1@gmail.com', '$2y$10$Ep0QDks03ztbm63LDl24geAPCn/BkNVG66fPN8QY3FZyQPKheAzFm'),
(25, 5, NULL, 'pelanggan1', 'pelanggan1@gmail.com', '$2y$10$6G6B/IhPV3ePnz49XtkwmOTNTP6BwNVtQQslN.NBj8twUToDHlsT6'),
(26, 5, NULL, 'pelanggan2', 'pelanggan2@gmail.com', '$2y$10$5fkG4ZKfiBVPe06mOl2J7e4UNuU/jJVTVT5uizYJ6.8JL69CgXxwy'),
(27, 1, 'uploads/272.png', 'Muhammad Yusuf Maulana', 'admin1@gmail.com', '$2y$10$a8XpyQ9IDjCZ1YXIGuuOluaONRXvNgNgpJuc/PdejcfYMAliEm1fW'),
(30, 5, NULL, 'pelanggan3', 'pelanggan3@gmail.com', '$2y$10$dCeAEPTV7q/xyFGHLrJdM.sNpyt2DoCmxmtV412yrb0v9b08eO0L.'),
(31, 5, NULL, 'pelanggan4', 'pelanggan4@gmail.com', '$2y$10$K.ArhrFSQ6V.Q6XHVY8utOnMvYMsXM1xgVSvjRnY7DW/NJM.QfDPS'),
(32, 5, NULL, 'pelanggan5', 'pelanggan5@gmail.com', '$2y$10$na.dwlNiiL9NM017OeuCqOOGXlEkp9sseCsugxwJmnGao8A67TBBq'),
(33, 5, NULL, 'pelanggan6', 'pelanggan6@gmail.com', '$2y$10$u0ON06AQy6P.xLgXjfLkBuY67J78nFRO/TpKq/lgnthEU3vVVaoIO');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_level` int(5) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_edit_transaksi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_edit_transaksi` (
`id` int(11)
,`id_user` int(11)
,`id_menu` int(11)
,`foto` varchar(255)
,`nama_menu` varchar(100)
,`id_detail_order` int(11)
,`jumlah` int(11)
,`sub_total` varchar(100)
,`total_bayar` varchar(100)
,`status_detail_order` varchar(30)
,`id_transaksi` int(11)
,`dibayar` varchar(100)
,`history` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_insert_transaksi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_insert_transaksi` (
`id` int(11)
,`id_menu` int(11)
,`foto` varchar(255)
,`nama_menu` varchar(100)
,`id_detail_order` int(11)
,`jumlah` int(11)
,`sub_total` varchar(100)
,`total_bayar` varchar(100)
,`status_detail_order` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_insert_transaksi_old`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_insert_transaksi_old` (
`id` int(11)
,`nama_menu` varchar(100)
,`foto` varchar(255)
,`id_menu` int(11)
,`id_order` int(11)
,`jumlah` int(11)
,`sub_total` varchar(100)
,`status_detail_order` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_list_order_waiter`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_list_order_waiter` (
`id` int(11)
,`id_user` int(11)
,`nama_pelanggan` varchar(100)
,`no_meja` enum('01','02','03','04','05','06','07','08','09','10','11','12','13')
,`waktu_order` varchar(100)
,`catatan` text
,`status_order` varchar(20)
,`status_detail_order` varchar(30)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_menu`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_menu` (
`id` int(11)
,`nama_menu` varchar(100)
,`status_menu` enum('TERSEDIA','TIDAK TERSEDIA')
,`nama_kategori` varchar(100)
,`harga` varchar(100)
,`deskripsi` varchar(255)
,`foto` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_order`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_order` (
`id` int(11)
,`nama_pelanggan` varchar(100)
,`no_meja` enum('01','02','03','04','05','06','07','08','09','10','11','12','13')
,`waktu_order` varchar(100)
,`catatan` text
,`status_order` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_user`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_user` (
`id` int(11)
,`nama` varchar(50)
,`email` varchar(100)
,`nama_level` varchar(100)
,`foto` varchar(255)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `four_table`
--
DROP TABLE IF EXISTS `four_table`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `four_table`  AS  select `tb_order`.`id` AS `id`,`tb_order`.`id_user` AS `id_user`,`tb_order`.`nama_pelanggan` AS `nama_pelanggan`,`tb_order`.`no_meja` AS `no_meja`,`tb_order`.`waktu_order` AS `waktu_order`,`tb_order`.`catatan` AS `catatan`,`tb_order`.`status_order` AS `status_order`,`tb_menu`.`id` AS `id_menu`,`tb_menu`.`id_kategori` AS `id_kategori`,`tb_menu`.`nama_menu` AS `nama_menu`,`tb_menu`.`harga` AS `harga`,`tb_menu`.`status_menu` AS `status_menu`,`tb_menu`.`deskripsi` AS `deskripsi`,`tb_menu`.`foto` AS `foto`,`tb_detail_order`.`id` AS `id_detail_order`,`tb_detail_order`.`jumlah` AS `jumlah`,`tb_detail_order`.`sub_total` AS `sub_total`,`tb_detail_order`.`total_bayar` AS `total_bayar`,`tb_detail_order`.`status_detail_order` AS `status_detail_order`,`tb_transaksi`.`id` AS `id_transaksi`,`tb_transaksi`.`dibayar` AS `dibayar`,`tb_transaksi`.`history` AS `history` from (((`tb_order` join `tb_detail_order` on((`tb_order`.`id` = `tb_detail_order`.`id_order`))) join `tb_menu` on((`tb_menu`.`id` = `tb_detail_order`.`id_menu`))) join `tb_transaksi` on((`tb_transaksi`.`id_order` = `tb_order`.`id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_edit_transaksi`
--
DROP TABLE IF EXISTS `v_edit_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_edit_transaksi`  AS  select `tb_order`.`id` AS `id`,`tb_order`.`id_user` AS `id_user`,`tb_menu`.`id` AS `id_menu`,`tb_menu`.`foto` AS `foto`,`tb_menu`.`nama_menu` AS `nama_menu`,`tb_detail_order`.`id` AS `id_detail_order`,`tb_detail_order`.`jumlah` AS `jumlah`,`tb_detail_order`.`sub_total` AS `sub_total`,`tb_detail_order`.`total_bayar` AS `total_bayar`,`tb_detail_order`.`status_detail_order` AS `status_detail_order`,`tb_transaksi`.`id` AS `id_transaksi`,`tb_transaksi`.`dibayar` AS `dibayar`,`tb_transaksi`.`history` AS `history` from (((`tb_order` join `tb_detail_order` on((`tb_order`.`id` = `tb_detail_order`.`id_order`))) join `tb_menu` on((`tb_menu`.`id` = `tb_detail_order`.`id_menu`))) join `tb_transaksi` on((`tb_transaksi`.`id_order` = `tb_detail_order`.`id_order`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_insert_transaksi`
--
DROP TABLE IF EXISTS `v_insert_transaksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_insert_transaksi`  AS  select `tb_order`.`id` AS `id`,`tb_menu`.`id` AS `id_menu`,`tb_menu`.`foto` AS `foto`,`tb_menu`.`nama_menu` AS `nama_menu`,`tb_detail_order`.`id` AS `id_detail_order`,`tb_detail_order`.`jumlah` AS `jumlah`,`tb_detail_order`.`sub_total` AS `sub_total`,`tb_detail_order`.`total_bayar` AS `total_bayar`,`tb_detail_order`.`status_detail_order` AS `status_detail_order` from ((`tb_order` join `tb_detail_order` on((`tb_order`.`id` = `tb_detail_order`.`id_order`))) join `tb_menu` on((`tb_menu`.`id` = `tb_detail_order`.`id_menu`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_insert_transaksi_old`
--
DROP TABLE IF EXISTS `v_insert_transaksi_old`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_insert_transaksi_old`  AS  select `tb_detail_order`.`id` AS `id`,`tb_menu`.`nama_menu` AS `nama_menu`,`tb_menu`.`foto` AS `foto`,`tb_detail_order`.`id_menu` AS `id_menu`,`tb_detail_order`.`id_order` AS `id_order`,`tb_detail_order`.`jumlah` AS `jumlah`,`tb_detail_order`.`sub_total` AS `sub_total`,`tb_detail_order`.`status_detail_order` AS `status_detail_order` from (`tb_detail_order` join `tb_menu` on((`tb_detail_order`.`id_menu` = `tb_menu`.`id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_list_order_waiter`
--
DROP TABLE IF EXISTS `v_list_order_waiter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_list_order_waiter`  AS  select `tb_order`.`id` AS `id`,`tb_order`.`id_user` AS `id_user`,`tb_order`.`nama_pelanggan` AS `nama_pelanggan`,`tb_order`.`no_meja` AS `no_meja`,`tb_order`.`waktu_order` AS `waktu_order`,`tb_order`.`catatan` AS `catatan`,`tb_order`.`status_order` AS `status_order`,`tb_detail_order`.`status_detail_order` AS `status_detail_order` from (`tb_order` join `tb_detail_order` on((`tb_order`.`id` = `tb_detail_order`.`id_order`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_menu`
--
DROP TABLE IF EXISTS `v_menu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_menu`  AS  select `tb_menu`.`id` AS `id`,`tb_menu`.`nama_menu` AS `nama_menu`,`tb_menu`.`status_menu` AS `status_menu`,`tb_kategori`.`nama_kategori` AS `nama_kategori`,`tb_menu`.`harga` AS `harga`,`tb_menu`.`deskripsi` AS `deskripsi`,`tb_menu`.`foto` AS `foto` from (`tb_menu` join `tb_kategori` on((`tb_menu`.`id_kategori` = `tb_kategori`.`id_kategori`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_order`
--
DROP TABLE IF EXISTS `v_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_order`  AS  select `tb_order`.`id` AS `id`,`tb_order`.`nama_pelanggan` AS `nama_pelanggan`,`tb_order`.`no_meja` AS `no_meja`,`tb_order`.`waktu_order` AS `waktu_order`,`tb_order`.`catatan` AS `catatan`,`tb_order`.`status_order` AS `status_order` from `tb_order` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user`  AS  select `tb_user`.`id` AS `id`,`tb_user`.`nama` AS `nama`,`tb_user`.`email` AS `email`,`tb_level`.`nama_level` AS `nama_level`,`tb_user`.`foto` AS `foto` from (`tb_user` join `tb_level` on((`tb_user`.`id_level` = `tb_level`.`id_level`))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_detail_order`
--
ALTER TABLE `tb_detail_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`),
  ADD KEY `id_order` (`id_order`);

--
-- Indeks untuk tabel `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indeks untuk tabel `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_order` (`id_order`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_level` (`id_level`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_detail_order`
--
ALTER TABLE `tb_detail_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_detail_order`
--
ALTER TABLE `tb_detail_order`
  ADD CONSTRAINT `tb_detail_order_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `tb_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_detail_order_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD CONSTRAINT `tb_menu_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_order`
--
ALTER TABLE `tb_order`
  ADD CONSTRAINT `tb_order_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD CONSTRAINT `tb_transaksi_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_transaksi_ibfk_2` FOREIGN KEY (`id_order`) REFERENCES `tb_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `tb_level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
