const flashData = $('.flash-data').data('flashdata');

if (flashData) {
	Swal({
		title: 'Data Event',
		text: 'Berhasil ' + flashData,
		type: 'success'
	})
}

// tombol hapus
$('.tombol-close').on('click', function (e) {
	e.preventDefault();
	const href = $(this).attr('href');

	Swal({
		title: 'Apakah Anda Yakin?',
		text: "Anda Akan Mengeluarkan Akun",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Keluar!'
	}).then((result) => {
		if (result.value) {

			document.location.href = href;

		}
	})
})