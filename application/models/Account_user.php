<?php
class Account_user extends CI_Model{
  
  private $db2;
	private $db3;
	private $db4;
	private $db5;
	private $db6;
	public function __construct()
	{	
		parent::__construct();
		$this->db2 = $this->load->database('admin', TRUE);
		$this->db3 = $this->load->database('own', TRUE);
		$this->db4 = $this->load->database('kasir', TRUE);
		$this->db5 = $this->load->database('wtr', TRUE);
		$this->db6 = $this->load->database('plg', TRUE);
	}

	// cek nip dan pass dosen
	public function auth_administrator($email,$pass) {
		$query=$this->db2->query("SELECT * FROM tb_user WHERE email='$email' AND pass=MD5('$pass') ");
		return $query;
    }
    public function auth_owner($email,$pass) {
		$query=$this->db3->query("SELECT * FROM tb_user WHERE email='$email' AND pass=MD5('$pass') ");
		return $query;
    }
    public function auth_kasir($email,$pass) {
		$query=$this->db4->query("SELECT * FROM tb_user WHERE email='$email' AND pass=MD5('$pass') ");
		return $query;
    }
    public function auth_waiter($email,$pass) {
		$query=$this->db5->query("SELECT * FROM tb_user WHERE email='$email' AND pass=MD5('$pass') ");
		return $query;
    }
    public function auth_pelanggan($email,$pass) {
		$query=$this->db6->query("SELECT * FROM tb_user WHERE email='$email' AND pass=MD5('$pass') ");
		return $query;
	}

    public function daftarUser() 
    {
    	$data = [
    				"nama" => $this->input->post('nama', true),
            "email" => $this->input->post('email', true),
            "pass" => password_hash($this->input->post('pass'),PASSWORD_DEFAULT),
            "id_level" => $this->input->post('id_level', true)

    			]; 
 
        $this->db->insert('tb_user', $data); 
    } 

    public function daftarMember() 
	  {
        $data = [
    				"nama" => $this->input->post('nama', true),
            "email" => $this->input->post('email', true),
            "pass" => password_hash($this->input->post('pass'),PASSWORD_DEFAULT),
            "id_level" => $this->input->post('id_level', true)
    			];  

    	  $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Regis/form_khusus', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            if($this->db2->insert('tb_user', $data)) {
                redirect('perusahaan');
            }
        }
    }

    public function ubahProfileAdmin()
	  {
        $data = [
            "nama" => $this->input->post('nama', true),
            "email" => $this->input->post('email', true),
            "pass" => password_hash($this->input->post('pass'),PASSWORD_DEFAULT)
						
    		]; 
		
	    	$config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Template/header', $error); 
    				$this->load->view('PROFIL/edit', $error);	
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            $this->db2->where('id', $this->input->post('id'));
            if($this->db2->update('tb_user', $data)) {
                redirect('profile');
            } 
        }

        $this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_user', $data);
    }


} 