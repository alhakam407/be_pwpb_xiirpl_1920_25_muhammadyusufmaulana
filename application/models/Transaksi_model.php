<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Transaksi_model extends CI_Model {

	private $db2;
	private $db3;
	private $db4;
	private $db5;
	private $db6;
	public function __construct()
	{	
		parent::__construct();
		$this->db2 = $this->load->database('admin', TRUE);
		$this->db3 = $this->load->database('own', TRUE);
		$this->db4 = $this->load->database('kasir', TRUE);
		$this->db5 = $this->load->database('wtr', TRUE);
		$this->db6 = $this->load->database('plg', TRUE);
	}

	public function cariData() { 
    	$keyword = $this->input->post('keyword', true);
    	$this->db2->like('nama_pelanggan', $keyword); 
    	$this->db2->or_like('no_meja', $keyword);
    	$this->db2->or_like('waktu_order', $keyword);
    	$this->db2->or_like('status_order', $keyword);
    	return $this->db2->get('tb_order')->result_array();
	}
	
	public function get_transaksi_list()
	{ 
		$this->db->where('history','TAMPILKAN');
		$query = $this->db->get('tb_transaksi');
		return $query->result_array();
    }  
    
    public function get_order_list_admin($limit, $start)
	{ 
		$query = $this->db2->get('tb_order', $limit, $start)->result_array(); 
		return $query;
	}
	public function get_order_list_kasir() 
	{
		$this->db->where('history', 'TAMPILKAN');	 
		// $this->db->where('status_order', 'Dibayar');
		// $this->db->where('status_pesanan', 'DALAM PROSES');
		$query = $this->db->get('tb_order')->result_array();
		return $query;
	}
	public function get_order_list_waiter()
	{ 
		$this->db4->where('status_order', 'Dibayar');
		$this->db4->where('status_pesanan', 'DALAM PROSES');
		$query = $this->db4->get('tb_order')->result_array();
		return $query;
	}
	
	public function statusDetailOrder()
	{
		$query = $this->db->get('tb_detail_order');
		return $query->result_array();
	}
    
	public function insert_transaksi($id){
		$query = $this->db->get_where('v_insert_transaksi', ['id' => $id])->result_array();
		return $query;
	}

	public function edit_transaksi($id){
		$query = $this->db->get_where('v_edit_transaksi', ['id' => $id])->result_array();
		return $query;
	}

	public function detail_transaksi($id){
		$query = $this->db->get_where('four_table', ['id_transaksi' => $id])->result_array();
		return $query;
	}

	public function detail_order($id){
		$query = $this->db->get_where('four_table', ['id' => $id])->result_array();
		return $query;
	}

	public function inputTransaksi(){
		$data= [ 
			"id_user" => $this->input->post('id_user', true),
			"id_order" => $this->input->post('id_order', true),
			"total_bayar" => $this->input->post('total_bayar', true),
			"dibayar" => $this->input->post('dibayar', true),
			"history" => $this->input->post('history', true)
		];

		$this->db->insert('tb_transaksi', $data);
	}

	public function editTransaksi(){
		$data= [
			"dibayar" => $this->input->post('dibayar', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('tb_transaksi', $data);
	}

	public function softDeleteOrder(){
		$data= [
			"history" => $this->input->post('history', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('tb_order', $data);
	}

	public function softDeleteTransaksi(){
		$data= [
			"history" => $this->input->post('history', true)
		];

		$this->db->where('id', $this->input->post('id'));
		$this->db->update('tb_transaksi', $data);
	}
    
    public function ubahDataOrder()
	{
        $data = [
    		"nama_level" => $this->input->post('nama_level', true)
    		]; 
		
		$this->db->where('id', $this->input->post('id'));
        $this->db->update('tb_user', $data);
	}
	
	public function hapusDataOrder($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tb_order');
	}

	public function get_menu_kategori($kategori)
	{ 
		if($kategori>0)
			{
				$this->db->where('id_kategori',$kategori);
			}
		$query = $this->db->get('tb_menu');
		return $query->result_array(); 
	}
	
	public function get_kategori_all()
	{
		$query = $this->db->get('tb_kategori');
		return $query->result_array();
	}
	
	public  function get_menu_id($id)
	{
		$this->db->select('tb_menu.*,nama_kategori');
		$this->db->from('tb_menu');
		$this->db->join('tb_kategori', 'id_kategori=tb_kategori.id_kategori','left');
   		$this->db->where('id',$id);
        return $this->db->get();
    }	
	
	public function inputorder($data){
		$this->db->insert('tb_order', $data);
		return $this->db->insert_id();
	}

	public function inputdetailorder($data){
		$this->db->insert('tb_detail_order', $data);
	}
}
?>