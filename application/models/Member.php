<?php

class Member extends CI_model {

    private $db2;
	private $db3;
	private $db4;
	private $db5;
	private $db6;
	public function __construct()
	{	 
		parent::__construct();
		$this->db2 = $this->load->database('admin', TRUE);
		$this->db3 = $this->load->database('own', TRUE);
		$this->db4 = $this->load->database('kasir', TRUE);
		$this->db5 = $this->load->database('wtr', TRUE);
		$this->db6 = $this->load->database('plg', TRUE);
    }
    
    function get_user_list($limit, $start){

        $this->db->where('nama_level', 'PELANGGAN');
    
        $query = $this->db->get('v_user', $limit, $start)->result_array();
        return $query;
    }
    
    public function cariUser() { 
    	$keyword = $this->input->post('keyword', true);
    	$this->db2->like('nama', $keyword); 
    	$this->db2->or_like('email', $keyword);
    	$this->db2->or_like('nama_level', $keyword);
    	return $this->db2->get('v_user')->result_array();
    }   

    public function getUserById($id) 
	{

		return $this->db2->get_where('tb_user', ['id' => $id])->row_array();

    } 
	
	public function getNamaLevel() {

        $this->db2->select('id_level, nama_level');
        $this->db2->from('tb_level');
        $result = $this->db2->get()->result_array();
        return $result; 
	}
	
    public function ubahDataUser()
	{ 
        $data = [
    		"id_level" => $this->input->post('id_level', true)
    		]; 
		
		$this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_user', $data);
    }

    public function hapusDataUser($id)
	{
		$this->db2->where('id', $id);
		$this->db2->delete('tb_user');
	}
 
    public function getAllMember1() 
	{
        $pelanggan= 5;
        $where = ['id_level !='=> $pelanggan];
        return $this->db2->get_where('tb_user',$where)->result_array();
	} 
	public function getAllMember2() 
	{
        $pelanggan= 'PELANGGAN';
        $where = ['nama_level !='=> $pelanggan];
        return $this->db2->get_where('v_user',$where)->result_array();
    }
	public function getAllWaiter() 
	{
        $this->db->where('nama_level', 'WAITER');
		return $this->db->get('v_user')->result_array();
    }  
    public function getAllKasir() 
	{
        $this->db->where('nama_level', 'KASIR');
		return $this->db->get('v_user')->result_array();
    }  
    public function getAllOwner() 
	{
        $this->db->where('nama_level', 'OWNER');
		return $this->db->get('v_user')->result_array();
	}  
	
}