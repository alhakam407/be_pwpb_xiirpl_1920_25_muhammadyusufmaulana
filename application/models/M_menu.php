<?php
class M_menu extends CI_Model{
	

    private $db2;
	private $db3;
	private $db4;
	private $db5;
	private $db6;
	public function __construct()
	{	
		parent::__construct();
		$this->db2 = $this->load->database('admin', TRUE);
		$this->db3 = $this->load->database('own', TRUE);
		$this->db4 = $this->load->database('kasir', TRUE);
		$this->db5 = $this->load->database('wtr', TRUE);
		$this->db6 = $this->load->database('plg', TRUE);
	} 

    function getNewMenu(){
        $query = $this->db->query("SELECT nama_menu,foto,harga, MAX(id) AS id_menu FROM tb_menu GROUP BY nama_menu");
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    function get_all_menu(){

        $this->db2->where('nama_kategori', 'MAKANAN');
     
        $query = $this->db2->get('v_menu')->result_array();
        return $query;
    }
    function get_makanan_list(){

        $this->db2->where('nama_kategori', 'MAKANAN');
    
        $query = $this->db2->get('v_menu')->result_array();
        return $query;
    } 
    function get_minuman_list(){

        $this->db2->where('nama_kategori', 'MINUMAN');
    
        $query = $this->db2->get('v_menu')->result_array();
        return $query;
    }
    function get_pencucimulut_list(){

        $this->db2->where('nama_kategori', 'PENCUCI MULUT');
    
        $query = $this->db2->get('v_menu')->result_array();
        return $query;
    }
    function get_pakethemat_list(){

        $this->db2->where('nama_kategori', 'PAKET HEMAT');
    
        $query = $this->db2->get('v_menu')->result_array();
        return $query;
    }
    function get_paketbesar_list(){

        $this->db2->where('nama_kategori', 'PAKET BESAR');
    
        $query = $this->db2->get('v_menu')->result_array();
        return $query;
    }
    
    public function cariDataMenu() {

        $keyword = $this->input->post('keyword', true);
        $this->db2->like('nama_menu', $keyword);
        $this->db2->or_like('harga', $keyword);
        $this->db2->or_like('status_menu', $keyword);

        return $this->db2->get('tb_menu')->result_array();
    }

    public function tambahDataMenu()
	{
        $data = [
    				"id_kategori" => $this->input->post('id_kategori', true),
                    "nama_menu" => $this->input->post('nama_menu', true),
                    "harga" => $this->input->post('harga', true),
                    "status_menu" => $this->input->post('status_menu', true),
                    "deskripsi" => $this->input->post('deskripsi', true)
    			];   

    	$config['upload_path'] = './uploads/'; 
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/makanan', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            if($this->db2->insert('tb_menu', $data)) {
                redirect('menu');
            }
        }
    }
    public function ubahDataMenu()
	{
        $data = [
            "nama_menu" => $this->input->post('nama_menu', true),
            "status_menu" => $this->input->post('status_menu', true),
    		"id_kategori" => $this->input->post('id_kategori', true),
    		"harga" => $this->input->post('harga', true),
    		"deskripsi" => $this->input->post('deskripsi', true)
    		// "foto" => $this->input->post('foto', true)
						
    		]; 
		
		$config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/makanan', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            $this->db2->where('id', $this->input->post('id'));
            if($this->db2->update('tb_menu', $data)) {
                redirect('menu');
            } 
        }

        $this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_menu', $data);
    }

    public function tambahDataMinuman()
	{
        $data = [
    				"id_kategori" => $this->input->post('id_kategori', true),
                    "nama_menu" => $this->input->post('nama_menu', true),
                    "harga" => $this->input->post('harga', true),
                    "status_menu" => $this->input->post('status_menu', true),
                    "deskripsi" => $this->input->post('deskripsi', true)
    			];  

    	$config['upload_path'] = './uploads/'; 
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/index', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            if($this->db2->insert('tb_menu', $data)) {
                redirect('minuman');
            }
        }
    }
    public function ubahDataMinuman()
	{
        $data = [
            "nama_menu" => $this->input->post('nama_menu', true),
            "status_menu" => $this->input->post('status_menu', true),
    		"id_kategori" => $this->input->post('id_kategori', true),
    		"harga" => $this->input->post('harga', true),
    		"deskripsi" => $this->input->post('deskripsi', true)
						
    		]; 
		
		$config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/minuman', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            $this->db2->where('id', $this->input->post('id'));
            if($this->db2->update('tb_menu', $data)) {
                redirect('minuman');
            } 
        }

        $this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_menu', $data);
    }

    public function tambahDataPencuciMulut()
	{
        $data = [
    				"id_kategori" => $this->input->post('id_kategori', true),
                    "nama_menu" => $this->input->post('nama_menu', true),
                    "harga" => $this->input->post('harga', true),
                    "status_menu" => $this->input->post('status_menu', true),
                    "deskripsi" => $this->input->post('deskripsi', true)
    			];  

    	$config['upload_path'] = './uploads/'; 
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/pencuci_mulut', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            if($this->db2->insert('tb_menu', $data)) {
                redirect('pencucimulut');
            }
        }
    }
    public function ubahDataPencuciMulut()
	{
        $data = [
            "nama_menu" => $this->input->post('nama_menu', true),
            "status_menu" => $this->input->post('status_menu', true),
    		"id_kategori" => $this->input->post('id_kategori', true),
    		"harga" => $this->input->post('harga', true),
    		"deskripsi" => $this->input->post('deskripsi', true)
						
    		]; 
		
		$config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/pencuci_mulut', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            $this->db2->where('id', $this->input->post('id'));
            if($this->db2->update('tb_menu', $data)) {
                redirect('pencucimulut');
            } 
        }

        $this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_menu', $data);
    }

    public function tambahDataPaketHemat()
	{
        $data = [
    				"id_kategori" => $this->input->post('id_kategori', true),
                    "nama_menu" => $this->input->post('nama_menu', true),
                    "harga" => $this->input->post('harga', true),
                    "status_menu" => $this->input->post('status_menu', true),
                    "deskripsi" => $this->input->post('deskripsi', true)
    			];  

    	$config['upload_path'] = './uploads/'; 
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/paket_hemat', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            if($this->db2->insert('tb_menu', $data)) {
                redirect('pakethemat');
            }
        }
    }
    public function ubahDataPaketHemat()
	{
        $data = [
            "nama_menu" => $this->input->post('nama_menu', true),
            "status_menu" => $this->input->post('status_menu', true),
    		"id_kategori" => $this->input->post('id_kategori', true),
    		"harga" => $this->input->post('harga', true),
    		"deskripsi" => $this->input->post('deskripsi', true)
						
    		]; 
		
		$config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/paket_hemat', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            $this->db2->where('id', $this->input->post('id'));
            if($this->db2->update('tb_menu', $data)) {
                redirect('pakethemat');
            } 
        }

        $this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_menu', $data);
    }

    public function tambahDataPaketBesar()
	{
        $data = [
    				"id_kategori" => $this->input->post('id_kategori', true),
                    "nama_menu" => $this->input->post('nama_menu', true),
                    "harga" => $this->input->post('harga', true),
                    "status_menu" => $this->input->post('status_menu', true),
                    "deskripsi" => $this->input->post('deskripsi', true)
    			];  

    	$config['upload_path'] = './uploads/'; 
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/paket_besar', $error);
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            if($this->db2->insert('tb_menu', $data)) {
                redirect('paketbesar');
            }
        }
    }
    public function ubahDataPaketBesar()
	{
        $data = [
            "nama_menu" => $this->input->post('nama_menu', true),
            "status_menu" => $this->input->post('status_menu', true),
    		"id_kategori" => $this->input->post('id_kategori', true),
    		"harga" => $this->input->post('harga', true),
    		"deskripsi" => $this->input->post('deskripsi', true)
						
    		]; 
		
		$config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $this->input->post('id');

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('foto') ) {
            $error = ['error' => $this->upload->display_errors()];
            $this->load->view('Menu/paket_besar', $error); 
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $data['foto'] = 'uploads/'.$file_name;
            $this->db2->where('id', $this->input->post('id'));
            if($this->db2->update('tb_menu', $data)) {
                redirect('paketbesar');
            } 
        }

        $this->db2->where('id', $this->input->post('id'));
        $this->db2->update('tb_menu', $data);
    }

    public function getMenuById($id) 
	{ 

		return $this->db->get_where('v_menu', ['id' => $id])->row_array();

    }

    

    public function getNamaKategori() {

        $this->db->select('id_kategori, nama_kategori');
        $this->db->from('tb_kategori');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function hapusDataMenu($id)
	{
		$this->db2->where('id', $id);
		$this->db2->delete('tb_menu');
	}
} 