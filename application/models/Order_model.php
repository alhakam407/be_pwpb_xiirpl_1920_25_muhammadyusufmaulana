<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Order_model extends CI_Model {

	private $db2;
	private $db3;
	private $db4;
	private $db5;
	private $db6;
	public function __construct()
	{	 
		parent::__construct();
		$this->db2 = $this->load->database('admin', TRUE);
		$this->db3 = $this->load->database('own', TRUE);
		$this->db4 = $this->load->database('kasir', TRUE);
		$this->db5 = $this->load->database('wtr', TRUE);
		$this->db6 = $this->load->database('plg', TRUE);
	} 

	public function get_menu_all()
	{ 
		$query = $this->db->get('tb_menu');
		return $query->result_array();
	}
	
	public function getOrderById($id)
	{
		$query = $this->db->get_where('tb_order', ['id' => $id])->result_array();
		return $query;
	}

	public function getDetailOrder()
	{
		$query = $this->db->get('tb_detail_order');
		return $query->result_array();
	}
	
	public function get_menu_kategori($kategori)
	{
		if($kategori>0)
			{
				$this->db->where('id_kategori',$kategori);
			}
		$query = $this->db->get('tb_menu');
		return $query->result_array(); 
	} 
	  
	public function get_kategori_all()
	{ 
		$query = $this->db->get('tb_kategori');
		return $query->result_array();
	}
	
	public  function get_menu_id($id)
	{
		$this->db->select('tb_menu.*,nama_kategori');
		$this->db->from('tb_menu');
		$this->db->join('tb_kategori', 'id_kategori=tb_kategori.id_kategori','left');
   		$this->db->where('id',$id);
        return $this->db->get();
    }	
	 
	public function inputorderadmin($data){
		$this->db2->insert('tb_order', $data);
		return $this->db2->insert_id();
	}
	public function inputorderwaiter($data){
		$this->db5->insert('tb_order', $data);
		return $this->db5->insert_id();
	}
	public function inputorderpelanggan($data){
		$this->db6->insert('tb_order', $data);
		return $this->db6->insert_id();
	}

	public function inputdetailorderadmin($data){
		$this->db2->insert('tb_detail_order', $data);
	}
	public function inputdetailorderwaiter($data){
		$this->db5->insert('tb_detail_order', $data);
	}
	public function inputdetailorderpelanggan($data){
		$this->db6->insert('tb_detail_order', $data);
	}

	public function editStatusPesanan(){
		$data = [
			"status_pesanan" => $this->input->post('status_pesanan', true)
		];

		$this->db->where('id', $this->input->post('id'));
        $this->db->update('tb_order', $data);
	}
}
?>