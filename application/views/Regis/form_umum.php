<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>Registrasi | Siswa</title>
    <link rel="icon" type="image/png" href="assets/img/logo.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/absen.png" type="image/x-icon" /> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/regis.css">
</head>

<body>
    <div class="login">
        <h2>Form Registrasi Akun Pelanggan</h2>
        <form action="" method="post" enctype="multipart/form-data">
            <!-- <div class="input-group form-group">
                <div class="custom-file">
                    <input type="file" name="foto" class="custom-file-input font-default"
                                                   id="validatedCustomFile" required>
                    <label class="custom-file-label" for="validatedCustomFile">Choose Your
                                                    Picture</label>
                </div>
            </div> -->
            <div class="input-group form-group">
                <input type="text" name="nama" required="required">
                <span>Username</span> 
                <!-- <p> <?php echo form_error('nama'); ?> </p> -->
            </div>
            <div class="input-group form-group">
                <input type="email" name="email" required="required">
                <span>Email</span>
                <!-- <p> <?php echo form_error('email'); ?> </p> -->
            </div>
            <div class="input-group form-group">
                <input type="password" name="pass" required="required">
                <span>Password</span>
                <!-- <p> <?php echo form_error('pass'); ?> </p> -->
            </div>
            <div class="input-group">
                <input type="password" name="pass_conf" required="required">
                <span>konfirmasi Password</span>
                <!-- <p> <?php echo form_error('pass_conf'); ?> </p> -->
            </div>
            <div class="input-group">
                <input type="hidden" name="id_level" value="5">
                <!-- <span>LEVEL</span> -->
                <!-- <p> <?php echo form_error('id_level'); ?> </p> -->
            </div>
            <center>
                <button type="submit" name="daftar">Register sekarang</button>
            </center>
        </form>
        <a href="<?= base_url();?>Home" class="btn btn-danger float-left text-white">Kembali</a>
    </div>
</body>

</html> 