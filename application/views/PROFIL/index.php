    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <a href="javascript:window.history.go(-1);"> 
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/settings.png">
                <h3>YOUR <b><span class="text-primary">PROFILE</span></b></h3>
            </div>
            <div class="right-list1">
                <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search Menu..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form> -->
            </div>
        </div>

        <div class="list2-profile">
            <div class="box-foto">
                <center>
                    <img src="<?php echo $this->session->userdata('ses_foto');?>">
                </center>
            </div>
            <div class="box-profile">
                <div class="form-group">
                    <label class="text-white"><b>NAMA</b></label>
                    <input type="text" name="nama" class="form-control" value="<?php echo $this->session->userdata('ses_nama');?>" disabled>
                </div>                
                <div class="form-group">
                    <label class="text-white"><b>EMAIL</b></label>
                    <input type="text" name="nama" class="form-control" value="<?php echo $this->session->userdata('ses_email');?>" disabled>
                </div>
                <div class="form-group">
                    <label class="text-white"><b>LEVEL</b></label>
                    <input type="text" name="nama" class="form-control" value="<?php echo $this->session->userdata('ses_level');?>" disabled>
                </div>
                <a href="<?= base_url(); ?>profile/edit" class="btn btn-success mt-4">EDIT</a>
            </div>
        </div>
        
    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>