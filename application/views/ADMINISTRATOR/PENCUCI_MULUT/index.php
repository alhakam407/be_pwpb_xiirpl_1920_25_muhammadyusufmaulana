    <div id="content">    
         
        <div class="list1">
            <div class="right-list1">
                <a href="<?= base_url(); ?>Dashboard/admin"> 
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/menu.png">
                <h3>MENU <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
                <form action="" method="post" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" name="keyword" placeholder="Search Menu..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </div>
        
        <div class="list2">
            <a href="<?= base_url(); ?>menu"><h5>MAKANAN</h5></a>
            <a href="<?= base_url(); ?>minuman"><h5>MINUMAN</h5></a>
            <a href="<?= base_url(); ?>pencucuimulut" style="text-shadow: 100px;"><h5><b>PENCUCI MULUT</b></h5></a>
            <a href="<?= base_url(); ?>pakethemat"><h5>PAKET HEMAT</h5></a>
            <a href="<?= base_url(); ?>paketbesar"><h5>PAKET BESAR</h5></a>
        </div>

        <div class="content">

            <div class="container">
                <div class="row">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalplusmenu"> + TAMBAH MENU</button>
                </div>
            </div>
            <?php foreach( $menu as $pencuci_mulut ) :?>
            <a href="#" data-toggle="modal" data-target="#detailmenu<?php echo $pencuci_mulut['id']; ?>" class="box">
                <center>
                    <img src="<?php echo $pencuci_mulut['foto']; ?>">
                    <h5><?php echo $pencuci_mulut['harga']; ?></h5>
                    <p><?php echo $pencuci_mulut['nama_menu']; ?></p>
                </center>
            </a>
            <?php endforeach; ?>

            <!-- modal tambah menu -->
            <div class="modal fade bd-example-modal-lg" id="modalplusmenu" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title">Tambah Menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= base_url(); ?>pencucimulut/tambah" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="id_kategori">Nama Kategori</label><br>
                                        <select name="id_kategori" id="id_kategori" class="form-control">
                                            <option value="">--- Pilih Kategori ----</option>
                                            <option value="3">PENCUCI MULUT</option>
                                        </select>
                                        <small class="form-text text-danger"><?= form_error('id_kategori'); ?></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="status_menu">Status Menu</label><br>
                                        <select name="status_menu" id="status_menu" class="form-control">
                                            <option value="">--- Pilih Status ----</option>
                                            <option value="TERSEDIA">TERSEDIA</option>
                                            <option value="TIDAK TERSEDIA">TIDAK TERSEDIA</option>
                                        </select>
                                        <small class="form-text text-danger"><?= form_error('status_menu'); ?></small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_menu">Nama Menu</label>
                                        <input type="text" name="nama_menu" class="form-control" id="nama_menu" placeholder="Masukan Nama...">
                                        <small class="form-text text-danger"><?= form_error('nama_menu'); ?></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="harga">Harga Menu</label>
                                        <input type="text" name="harga" class="form-control" id="harga" placeholder="Masukan Nama...">
                                        <small class="form-text text-danger"><?= form_error('harga'); ?></small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="deskripsi" class="font-default">Deskripsi Menu</label><br>
                                    <textarea name="deskripsi" id="deskripsi" class="form-control" style="min-height: 100px !important;"
                                                   placeholder="Deskripsikan Menu"></textarea>    
                                </div>                  
                            </div>  
                            <div class="row mt-3">   
                                <div class="col-md-12">
                                    <div class="custom-file">
                                        <input type="file" name="foto" class="custom-file-input font-default"
                                                   id="validatedCustomFile" required>
                                        <label class="custom-file-label" for="validatedCustomFile">Choose Menu
                                                    Picture</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save Menu</button>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
            
            <!-- modal detail menu -->
            <?php foreach( $menu as $data_pencucimulut ) : ?> 
            <div class="modal fade bd-example-modal-lg" id="detailmenu<?php echo $data_pencucimulut['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= base_url(); ?>pencucimulut/ubah" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="isi-modal">
                                <div class="isi-right">
                                    <img src="<?php echo $data_pencucimulut['foto']; ?>">
                                </div>
                                <div class="isi-left">
                                    <h4><b><?php echo $data_pencucimulut['nama_menu']; ?></b></h4>
                                    <h5 class="d-inline-block text-white pt-1 pl-2 pb-1 pr-2"><?php echo $data_pencucimulut['harga']; ?></h5>
                                    <P><?php echo $data_pencucimulut['deskripsi']; ?></P>
                                    <div class="aksi">
                                        <a href="<?= base_url(); ?>pencucimulut/hapus/<?php echo $data_pencucimulut['id']; ?>" class="btn btn-outline-danger tombol-hapus">HAPUS</a>
                                        <a href="#" class="btn btn-outline-success ubah" data-toggle="modal" data-target="#modaleditmenu<?php echo $data_pencucimulut['id']; ?>" data-dismiss="#detailmenu">EDIT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>        
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
 
            <!-- modal edit menu -->
            <?php foreach( $menu as $data_pencucimulut ) : ?> 
            <div class="modal fade bd-example-modal-lg" id="modaleditmenu<?php echo $data_pencucimulut['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= base_url(); ?>pencucimulut/ubah" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo $data_pencucimulut['id']; ?>">
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="id_kategori">Nama Kategori</label><br>
                                        <select name="id_kategori" id="id_kategori" class="form-control">
                                            <option value="3">PENCUCI MULUT</option>
                                        </select>
                                        <small class="form-text text-danger"><?= form_error('id_kategori'); ?></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="status_menu">Status Menu</label><br>
                                        <select name="status_menu" id="status_menu" class="form-control">
                                            <?php foreach( $status_menu as $sm) : ?>
                                                <?php if( $sm == $data_pencucimulut['status_menu'] ) : ?>
                                                    <option value="<?= $sm; ?>" selected><?= $sm; ?></option>
                                                    <?php else : ?>
                                                    <option value="<?= $sm; ?>"><?= $sm; ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                        <small class="form-text text-danger"><?= form_error('status_menu'); ?></small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama_menu">Nama Menu</label>
                                        <input type="text" name="nama_menu" class="form-control" id="nama_menu" value="<?php echo $data_pencucimulut['nama_menu']; ?>">
                                        <small class="form-text text-danger"><?= form_error('nama_menu'); ?></small>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="harga">Harga Menu</label>
                                        <input type="text" name="harga" class="form-control" id="harga" value="<?php echo $data_pencucimulut['harga']; ?>">
                                        <small class="form-text text-danger"><?= form_error('harga'); ?></small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="deskripsi" class="font-default">Deskripsi Menu</label><br>
                                    <textarea name="deskripsi" id="deskripsi" class="form-control" style="min-height: 100px !important;"><?php echo $data_pencucimulut['deskripsi']; ?></textarea>    
                                </div>                  
                                <div class="col-md-6">
                                    <label class="font-default">Foto Menu</label><br>
                                    <div class="box-foto">
                                        <img src="<?php echo $data_pencucimulut['foto']; ?>">
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="foto" class="custom-file-input font-default"
                                                   id="validatedCustomFile">
                                        <label class="custom-file-label" for="validatedCustomFile">Choose Menu
                                                    Picture</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

        </div>

    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu<?php echo $menu['id']; ?>').modal('hide');
        });
    </script>
    <script src="<?= base_url(); ?>assets/js/btn.delete.sweetalert.js"></script>
    <script src="<?= base_url(); ?>assets/js/sweetalert2.all.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <!-- <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script> -->

</body>

</html>