    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <a href="<?= base_url(); ?>Dashboard/admin"> 
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/perusahaan.png">
                <h3>PERUSAHAAN <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
                <form action="" method="post" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" name="keyword" mr-sm-2" type="search" placeholder="Search..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </div>
        
        <div class="list2-list_transaksi">
            <a href="<?= base_url(); ?>perusahaan" class="btn btn-outline-primary">LIST MEMBER</a>
            <a style="color: #fff; padding: 10px 8px 10px 8px; border-radius: 3px; background-color:#00A0E8;">LIST USER</a>
            <a href="<?= base_url(); ?>Regis_khusus" class="btn btn-outline-primary" style="margin: 10px;"> + ADD MEMBER</a>
        </div>

        <!-- <div class="content"> -->
        <div class="table-responsive-vertical">
            <table class="table table-bordered table-striped table-hover table-mc-red" style="background-color: #fff !important;">
            <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>">
                <thead style="background-color: #fff !important;">
                    <tr>
                        <th>No</th>
                        <th>Foto</th>
                        <th>Nama</th>
                        <th>Email</th> 
                        <th>Level</th>
                        <th>Aksi</th>
                    </tr>
                </thead> 
                <tbody>
                    <?php $no = 1 ?>
                    <?php foreach( $data as $row ) : ?> 
                    <tr> 
                        <td data-title="No"><?= $no; ?></td>
                        <td data-title="Foto">
                                <!-- <img src="<?php echo $row['foto']; ?>" style="width: 60px;"> -->
                                <img src="<?= base_url(); ?>assets/img/logo.png" style="width: 60px;">
                        </td>
                        <td data-title="Nama"><?php echo $row['nama']; ?></td>
                        <td data-title="Email"><?php echo $row['email']; ?></td>
                        <td data-title="Level"><?php echo $row['nama_level']; ?></td>
                        <td data-title="Aksi"> 
                            <a href="<?= base_url(); ?>perusahaan/hapus/<?php echo $row['id']; ?>" class="btn btn-outline-danger tombol-hapus">DELETE</a>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div class="row">
				<div class="col">
					<?php if(!isset($_POST['keyword'])) : ?>
					<?php echo $pagination; ?>
					<?php endif; ?>
				</div>
			</div>
        </div>
        
    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>
    <script src="<?= base_url(); ?>assets/js/btn.delete.sweetalert.js"></script>
    <script src="<?= base_url(); ?>assets/js/sweetalert2.all.min.js"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <!-- <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script> -->

</body>

</html>