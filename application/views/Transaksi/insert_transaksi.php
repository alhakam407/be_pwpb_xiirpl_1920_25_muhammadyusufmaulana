    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <a href="javascript:window.history.go(-1);">  
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/kasir.png">
                <h3>INSERT TRANSAKSI <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
                <!-- <form action="" method="post" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" name="keyword" type="search" placeholder="Search..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form> -->
            </div>
        </div>
        
        <!-- <div class="content"> --> 
        <div class="content">
            <div class="table-responsive"> 
                <table class="table">
                    <thead style="background-color: #fff !important;">
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Nama Menu</th>
                            <th>QTY</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $no=1 ?>
                    <?php foreach($data3 as $dt3) :?>
                        <tr> 
                            <td data-title="No"><?= $no; ?></td>
                            <td data-title="Foto">
                                <img src="<?= base_url(). $dt3['foto']; ?>" style="width: 50px;">
                            </td>
                            <td data-title="Nama Menu"><?= $dt3['nama_menu']; ?></td>
                            <td data-title="QTY"><?= $dt3['jumlah']; ?></td>
                            <td data-title="Jumlah">Rp. <?= $dt3['sub_total']; ?></td>
                        </tr>
                    <?php $no++; ?>
                    <?php endforeach; ?>
                        <!-- <tr>
                            <td colspan="5"><hr/></td>
                        </tr> -->
                        <tr>    
                            <td colspan="5">
                                <div style="width: 100%;height: 2px;background-color: #333333;"></div>
                            </td>
                        </tr>
                    <form action="" method="post">
                        <input type="hidden" class="bg-primary" name="history" value="TAMPILKAN">
                        <input type="hidden" class="bg-primary" name="id_order" value="<?= $dt3['id'];?>">
                        <input type="hidden" class="bg-primary" name="id_user" value="<?php echo $this->session->userdata('ses_id');?>">
                        <input type="hidden" class="bg-primary" name="total_bayar" value="Rp. <?= $dt3['total_bayar'];?>">
                        <tr class="">
                            <td colspan="3">
                                <label><b>Total Yang Harus Anda Bayar:</b></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="Rp. <?= $dt3['total_bayar']; ?>" disabled/>
                                </div>
                            </td>
                            <td colspan="1">
                                <label><b>Masukkan Nominal Uang</b></label><br>
                                <div class="form-group">
                                    <input type="text" name="dibayar" class="form-control" value="Rp. ">
                                </div>
                            </td>
                            <td colspan="1">
                                <button type="submit" class="btn btn-primary mt-3 col-md-12">SUBMIT</button>
                            </td>
                    </form>
                        </tr>
                        
                    </tbody>

                </table>
            </div> 
                <!-- <div class="row">
                    <div class="col">
                        <?php if(!isset($_POST['keyword'])) : ?>
                        <?php echo $pagination; ?>
                        <?php endif; ?>
                    </div>
                </div> -->
        </div>
    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <!-- <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script> -->

</body>

</html>