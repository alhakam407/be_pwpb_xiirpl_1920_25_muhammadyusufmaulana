    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <a href="javascript:window.history.go(-1);">  
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/kasir.png">
                <h3>LIST ORDER <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
                <!-- <form action="" method="post" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" name="keyword" type="search" placeholder="Search..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form> -->
            </div>
        </div>
        
        <div class="list2-list_transaksi mb-4"> 
            <?php if($this->session->userdata('ses_level')=='ADMINISTRATOR') : ?>
            <a href="<?= base_url(); ?>Transaksi" class="btn btn-outline-primary p-3" style="margin: 10px;">LIST TRANSAKSI <img src="<?= base_url(); ?>assets/img/transaksi.png" style="width: 35px;"></a>
            <a style="color: #fff; padding: 20px 10px 20px 10px; border-radius: 3px; background-color:#00A0E8;">LIST ORDER <img src="<?= base_url(); ?>assets/img/kasir.png" style="width: 35px;"></a>
            <a href="<?= base_url(); ?>Transaksi/list_orderWAITER" class="btn btn-outline-primary p-3" style="margin: 10px;">LIST ORDER <img src="<?= base_url(); ?>assets/img/order.png" style="width: 35px;"></a>
            <?php elseif($this->session->userdata('ses_level')=='KASIR') : ?>
            <a href="<?= base_url(); ?>Transaksi" class="btn btn-outline-primary p-3" style="margin: 10px;">LIST TRANSAKSI <img src="<?= base_url(); ?>assets/img/transaksi.png" style="width: 35px;"></a>
            <a style="color: #fff; padding: 20px 10px 20px 10px; border-radius: 3px; background-color:#00A0E8;">LIST ORDER <img src="<?= base_url(); ?>assets/img/kasir.png" style="width: 35px;"></a>
            <?php endif;?>
        </div>

        <!-- <div class="content"> --> 
        <div class="table-responsive-vertical">
            <table id="example" class="datatable table">
                <thead>
                    <tr>
                        <th>No</th>
                        <!-- <th>Kode</th> -->
                        <th>Nama Pelanggan</th>
                        <th>No Meja</th>
                        <th>Waktu Order</th>
                        <th>Catatan</th>
                        <th>Status Transaksi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php $no = 1 ?>
                    <?php foreach( $data1 as $row) : ?>
                    <tr>  
                        <td data-title="No"><?= $no; ?></td>
                        <td data-title="Nama Pelanggan"><?= $row['nama_pelanggan']; ?></td>
                        <td data-title="No Meja"><?= $row['no_meja']; ?></td> 
                        <td data-title="Waktu Order"><?= $row['waktu_order']; ?></td>
                        <td data-title="Catatan"><?= $row['catatan']; ?></td>
                        <?php if( $row['status_order']=='Belum Dibayar'):?> 
                        <td data-title="Status Transaksi"><budge class="bg-warning text-white p-2" style="padding: 3px; border-radius: 3px;"><b><?= $row['status_order']; ?></b></budge></td>
                        <?php elseif( $row['status_order']=='Dibayar'): ?>
                        <td data-title="Status Transaksi"><budge class="bg-success text-white p-2" style="padding: 3px; border-radius: 3px;"><b><?= $row['status_order']; ?></b></budge></td>
                        <?php endif; ?>
                        
                        <td data-title="Aksi" class="d-flex justify-content-between">
                            <?php if( ($row['status_order']=='Belum Dibayar') && ($this->session->userdata('ses_level')=='ADMINISTRATOR')):?> 
                            <a href="<?= base_url(); ?>order/hapusorder/<?= $row['id']; ?>" class="btn btn-outline-danger m-1 p-1 tombol-hapus">HAPUS</a>
                            <a href="<?= base_url(); ?>transaksi/inserttransaksi/<?= $row['id']; ?>" class="btn btn-outline-primary m-1 p-1">INSERT TRANSAKSI</a>

                            <?php elseif( ($row['status_order']=='Dibayar') && ($this->session->userdata('ses_level')=='ADMINISTRATOR')): ?>
                            <form action="" method="post">
                            <input type="hidden" name="id" value="<?= $row['id']; ?>">
                            <input type="hidden" name="history" value="SEMBUNYIKAN">
                            <button type="submit" class="btn btn-outline-secondary">HIDDEN</button>
                            </form>
                            <a href="<?= base_url(); ?>transaksi/edittransaksiKASIR/<?= $row['id']; ?>" class="btn btn-outline-success">EDIT</a>
                            <a href="<?= base_url(); ?>transaksi/detailListOrder/<?= $row['id']; ?>" class="btn btn-outline-primary">DETAIL</a>

                            <?php elseif( ($row['status_order']=='Belum Dibayar') && ($this->session->userdata('ses_level')=='KASIR')):?> 
                            <a href="<?= base_url(); ?>order/hapusorder/<?= $row['id']; ?>" class="btn btn-outline-danger m-1 p-1 tombol-hapus">HAPUS</a>
                            <a href="<?= base_url(); ?>transaksi/inserttransaksi/<?= $row['id']; ?>" class="btn btn-outline-primary m-1 p-1">INSERT TRANSAKSI</a>
    
                            <?php elseif( ($row['status_order']=='Dibayar') && ($this->session->userdata('ses_level')=='KASIR')): ?>
                            <form action="" method="post">
                            <input type="hidden" name="id" value="<?= $row['id']; ?>">
                            <input type="hidden" name="history" value="SEMBUNYIKAN">
                            <button type="submit" class="btn btn-outline-secondary">HIDDEN</button>
                            </form>
                            <a href="<?= base_url(); ?>transaksi/edittransaksiKASIR/<?= $row['id']; ?>" class="btn btn-outline-success">EDIT</a>
                            <a href="<?= base_url(); ?>transaksi/detailListOrder/<?= $row['id']; ?>" class="btn btn-outline-primary">DETAIL</a>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>

            </table> 
               
        </div>

        
            
        
    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

        <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
        </script>
<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>

    <script src="<?= base_url(); ?>assets/js/btn.delete.sweetalert.js"></script>
    <script src="<?= base_url(); ?>assets/js/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <!-- <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script> -->

</body>

</html>