<div id="content">    
        
    <div class="list1">
        <div class="right-list1">
            <?php if( $this->session->userdata('ses_level')=='WAITER') : ?>
            <?php elseif( $this->session->userdata('ses_level')=='PELANGGAN'): ?>
            <?php elseif( $this->session->userdata('ses_level')=='ADMINISTRATOR'): ?>
            <a href="javascript:window.history.go(-1);"> 
                <img src="<?= base_url(); ?>assets/img/left-arrow.png">
            </a>
            <?php endif; ?>
            <img src="<?= base_url(); ?>assets/img/order.png">
            <h3>Order <b><span class="text-primary">MAMAM</span></b></h3>
        </div>
        <div class="right-list1">
            <!-- <form action="" method="post" class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" name="keyword" type="search" placeholder="Search Menu..." aria-label="Search">
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
            </form> -->
        </div>
    </div> 
        
    <div class="list2">
        <a href="<?php echo base_url()?>order"><h5><b>SEMUA</b></h5></a>
        <?php
		    foreach ($kategori as $row) 
            {
		?>
        <a href="<?php echo base_url()?>order/index/<?php echo $row['id_kategori'];?>"><h5 class="active"><b><?php echo $row['nama_kategori'];?></b></h5></a>
        <?php
			}
		?>  
    </div>

    <div class="content-order">
        <div class="content1-order">
            <?php if( $this->session->userdata('ses_level')=='ADMINISTRATOR') : ?>
            <a href="<?= base_url(); ?>Transaksi" class="btn btn-dark col-md-5 d-flex align-items-center justify-content-center m-3"><b>LIST ORDER</b></a>
            <a href="<?= base_url(); ?>Order/tampil_cart" class="btn btn-primary col-md-5 d-flex align-items-center justify-content-center m-3"><b>KERANJANG</b></a>
            <?php elseif( $this->session->userdata('ses_level')=='WAITER') : ?>
            <a href="<?= base_url(); ?>Transaksi/list_orderWAITER" class="btn btn-dark col-md-5 d-flex align-items-center justify-content-center m-3"><b>LIST ORDER</b></a>
            <a href="<?= base_url(); ?>Order/tampil_cart" class="btn btn-primary col-md-5 d-flex align-items-center justify-content-center m-3"><b>KERANJANG</b></a>
            <?php elseif( $this->session->userdata('ses_level')=='PELANGGAN') : ?>
            <a href="<?= base_url(); ?>Order/tampil_cart" class="btn btn-primary col-md-10 d-flex align-items-center justify-content-center m-3"><b>KERANJANG</b></a>

            <?php endif;?>
                
            <?php foreach( $menu as $order ) :?>
            <a href="#" data-toggle="modal" data-target="#detailmenu<?= $order['id']; ?>" class="box">
                <center>
                    <img src="<?= base_url() .$order['foto']; ?>"/>
                    <h5><?= $order['harga']; ?></h5>
                    <p><?= $order['nama_menu']; ?></p> 
                </center>
            </a>
            <?php endforeach; ?>
            
            <!-- modal detail menu order -->
            <?php foreach( $menu as $modal_order) :?>
            <div class="modal fade bd-example-modal-lg" id="detailmenu<?= $modal_order['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form action="<?php echo base_url();?>Order/tambah" method="post" accept-charset="utf-8">
                        <div class="modal-header">
                            <h5 class="modal-title">Detail Menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span> 
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="isi-modal">
                                <div class="isi-right">
                                    <img src="<?= base_url() . $modal_order['foto']; ?>">
                                </div>
                                <div class="isi-left">
                                    <h4><b><?= $modal_order['nama_menu']; ?></b></h4>
                                    <h5 class="d-inline-block text-white pt-1 pl-2 pb-1 pr-2"><?= $modal_order['harga'];?></h5><br/>
                                    <h5 class="d-inline-block text-white pt-1 pl-2 pb-1 pr-2"><?= $modal_order['status_menu'];?></h5>
                                    <P><?= $modal_order['deskripsi'];?></P>

                                    <input type="hidden" name="id" value="<?= $modal_order['id']; ?>" />
                                    <input type="hidden" name="foto" value="<?= $modal_order['foto']; ?>" />
                                    <input type="hidden" name="nama_menu" value="<?= $modal_order['nama_menu']; ?>" />
                                    <input type="hidden" name="harga" value="<?= $modal_order['harga']; ?>" />
                                    <input type="hidden" name="jumlah" value="1" />
                                <div class="aksi">
                                    <?php if ($modal_order['status_menu']=='TERSEDIA'):?>
                                    <button type="submit" class="btn btn-primary ubah btn-block" data-toggle="modal" data-target="#modaleditmenu<?php echo $modal_order['id']; ?>" data-dismiss="#detailmenu">PESAN MENU INI</button>
                                    <?php elseif ($modal_order['status_menu']=='TIDAK TERSEDIA'):?>
                                    <button type="button" class="btn btn-primary ubah btn-block" data-toggle="modal" data-target="#modaleditmenu<?php echo $modal_order['id']; ?>" data-dismiss="#detailmenu">MAAF MENU HABIS</button>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>        
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

        <div class="content2-order">
            <div class="header1-form p-4"><b>Keranjang <span>MAMAM</span></b></div>
            <?php
                $cart= $this->cart->contents();
                            
                if(empty($cart)) {
            ?>
                    <a href="#" class="list-group-item col-md-12 d-flex align-items-center justify-content-center p-2 text-dark">Keranjang Menu MAMAM Kosong</a>
            <?php        
                } else{
                    $grand_total = 0;
                    foreach ($cart as $cart_menu) {
                        $grand_total+=$cart_menu['subtotal'];
                        ?>
                        <a <a class="list-group-item col-md-12 d-flex align-items-center justify-content-center p-2 text-dark"><?php echo $cart_menu['name']; ?> (<?php echo $cart_menu['qty']; ?> x <?php echo number_format($cart_menu['price'],0,",","."); ?>) = <?php echo number_format($cart_menu['subtotal'],0,",","."); ?></a>    
                <?php
                        }
                    }
                
                ?>
            </div>
                 
        </div>
    </div>
    
    <!-- footer -->
        <div id="footer">
            <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                    GROUP</span></p>
            <p>Version 1.1</p>
        </div>
    
    <!-- sidebar -->
        <?php echo $scriptbuka; ?>
        <?php echo $scripttutup; ?>
        
    <!-- modal ubah -->
        <script>
            $('.ubah').click(function () {
                $('#detailmenu').modal('hide');
            });
        </script>
    
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
        </script> -->
        <script src="<?= base_url(); ?>assets/js/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
        <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script>
    
    </body>
    
    </html>