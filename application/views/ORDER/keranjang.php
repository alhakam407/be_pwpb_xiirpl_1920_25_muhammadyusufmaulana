    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <a href="javascript:window.history.go(-1);"> 
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/cart.png">
                <h3>Keranjang <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
                <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search Menu..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form> -->
            </div>
        </div> 
        
        <div class="content-keranjang p-3">
            <!-- <h2>Daftar Belanja</h2> -->
<form action="<?php echo base_url()?>Order/ubah_cart" method="post" name="frmShopping" id="frmShopping" class="form-horizontal" enctype="multipart/form-data">
<?php
	if ($cart = $this->cart->contents())
		{
 ?>
<div class="table-responsive">
<table class="table"> 
<tr id= "main_heading">
<td width="2%">No</td>
<td width="10%">Foto</td> 
<td width="33%">Nama Menu</td> 
<td width="17%">Harga</td>
<td width="8%">Qty</td>
<td width="20%">Jumlah</td>
<td width="10%">Hapus</td>
</tr>
<?php
// Create form and send all values in "shopping/update_cart" function.
$grand_total = 0;
$i = 1;

foreach ($cart as $item):
$grand_total = $grand_total + $item['subtotal'];
?> 

<input type="hidden" name="cart[<?php echo $item['id'];?>][id]" value="<?php echo $item['id'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][rowid]" value="<?php echo $item['rowid'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][name]" value="<?php echo $item['name'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][price]" value="<?php echo $item['price'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][foto]" value="<?php echo $item['foto'];?>" />
<input type="hidden" name="cart[<?php echo $item['id'];?>][qty]" value="<?php echo $item['qty'];?>" />
<input type="hidden" class="bg-dark" name="cart[<?php echo $item['id'];?>][subtotal]" value="<?php echo $item['subtotal'];?>" />

<tr>
<td><?php echo $i++; ?></td>
<td><img class="img-responsive" style="width: 80px !important;" src="<?php echo base_url() .$item['foto']; ?>"/></td>
<td><?php echo $item['name']; ?></td>
<td>Rp. <?php echo number_format($item['price'], 0,",","."); ?></td>
<td><input type="text" class="form-control input-sm" style="min-width: 35px !important;" name="cart[<?php echo $item['id'];?>][qty]" value="<?php echo $item['qty'];?>" /></td>
<td>Rp. <?php echo number_format($item['subtotal'], 0,",",".") ?></td>
<td><a href="<?php echo base_url()?>order/hapus/<?php echo $item['rowid'];?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a></td>
<?php endforeach; ?>
</tr>
<tr>
<td colspan="3"><b>Order Total: Rp <?php echo number_format($grand_total, 0,",","."); ?></b></td>
<td colspan="4" align="right">
<a data-toggle="modal" data-target="#myModal"  class ='btn btn-sm btn-danger text-white'>Kosongkan Cart</a>
<button class='btn btn-sm btn-success'  type="submit">Update Cart</button>
<a href="<?php echo base_url()?>Order/check_out"  class ='btn btn-sm btn-primary'>Check Out</a>
</tr>

</table>
</div>
<?php
		} 
	else
		{
			echo "<h3>Keranjang Belanja masih kosong</h3>";	
		}	
?>
</form>


  <!-- Modal Penilai -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" action="<?php echo base_url()?>order/hapus/all">
        <div class="modal-header">
          <button type="button" class="close float-right" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Konfirmasi</h4> -->
        </div>
        <div class="modal-body">
			Anda yakin ingin mengosongkan Keranjang <span style="color:#00A0E8;font-weight:bolder;">MAMAM</span> ?
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Tidak</button>
          <button type="submit" class="btn btn-sm btn-default">Ya</button>
        </div>
        
        </form>
      </div>
      
    </div> 
  </div>
  <!--End Modal-->
        </div>


        
            
        
    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>