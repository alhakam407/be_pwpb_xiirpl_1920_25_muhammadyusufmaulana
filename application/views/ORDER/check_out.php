    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <a href="javascript:window.history.go(-1);"> 
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png">
                </a>
                <img src="<?= base_url(); ?>assets/img/cart.png">
                <h3>Check Out Keranjang <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
         
            
            
            </div>
        </div>
        
        <div class="content-keranjang p-3">
        <form action="<?= base_url(); ?>Order/proses_order" method="post">

            <!-- <h2>Daftar Belanja</h2> -->
<!-- <form action="<?php echo base_url()?>Order/ubah_cart" method="post" name="frmShopping" id="frmShopping" class="form-horizontal" enctype="multipart/form-data"> -->
<?php
	if ($cart = $this->cart->contents())
		{ 
 ?>
<div class="table-responsive"> 
<table class="table">
<tr id= "main_heading">
<td width="2%">No</td>
<td width="10%">Foto</td> 
<td width="33%">Nama Menu</td>
<td width="17%">Harga</td>
<td width="8%">Qty</td>
<td width="20%">Jumlah</td>
<!-- <td width="10%">Hapus</td> -->
</tr>
<?php
// Create form and send all values in "shopping/update_cart" function.
$grand_total = 0;
$i = 1;

foreach ($cart as $item):
$grand_total = $grand_total + $item['subtotal'];
?>

<tr>
<td><?php echo $i++; ?></td>
<td><img class="img-responsive" style="width: 80px !important;" src="<?php echo base_url() .$item['foto']; ?>"/></td>
<td><?php echo $item['name']; ?></td>
<td>Rp. <?php echo number_format($item['price'], 0,",","."); ?></td>
<td><?php echo $item['qty']; ?></td>
<td>Rp. <?php echo number_format($item['subtotal'], 0,",",".") ?></td>
        <input type="hidden" class="bg-primary col-12" name="id_menu[]" value="<?php echo $item['id'];?>" />

<?php endforeach; ?>

</tr>
<tr>
<td colspan="3"><b>Total Yang Harus Anda Bayar: Rp <?php echo number_format($grand_total, 0,",","."); ?></b></td>
<td colspan="4" align="right">
</tr> 
 
</table>
</div>
        <input type="hidden" class="bg-primary col-12" name="id_user" value="<?php echo $this->session->userdata('ses_id');?>" />
        <input type="hidden" class="bg-primary col-12" name="price" value="<?php echo $item['price'];?>" />
        <input type="hidden" class="bg-primary col-12" name="sub_total" value="<?php echo $item['subtotal'];?>" />
        <input type="hidden" class="bg-primary col-12" name="jumlah" value="<?php echo $item['qty'];?>" />
        <input type="hidden" class="bg-dark col-12" name="total_bayar" value="<?php echo number_format($grand_total, 0,",",".");?>" />
        <?php
        }
?>
        <input type="hidden" class="bg-primary col-12" name="status_order" value="Belum Dibayar" />
        <input type="hidden" class="bg-primary col-12" name="status_detail_order" value="Telah Dipesan" />
        <input type="hidden" class="bg-primary col-12" name="status_pesanan" value="DALAM PROSES" />
        <input type="hidden" class="bg-primary col-12" name="history" value="TAMPILKAN" />
<div class="box-check_out p-2 d-flex justify-content-center flex-wrap">
        <div class="row col-md-4 pt-4" style="background-color: #C6E4FB;">
            <div class="form-group col-md-12">
                <label><b>Nama Pelanggan</b></label>
                <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $this->session->userdata('ses_nama');?>">
            </div>
            <div class="form-group col-md-12">
                <label><b>No Meja</b></label><br>
                <select name="no_meja" class="form-control">
                    <option>-- Silahkan Pilih No Meja--</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option> 
                </select>
            </div>
        </div>
        <div class="row col-md-4 pt-4" style="background-color: #C6E4FB;">
            <div class="form-group col-md-12">
                <label><b>Catatan</b></label>
                <textarea name="catatan" class="form-control" style="min-height: 210px !important;">Bismillah</textarea>    
            </div>
        </div>

        <div class="col-md-12 m-3 d-flex justify-content-center">
            <button type="submit" class="btn btn-primary col-md-5">SUBMIT!</button>
        </div>
    </form>
</div>
<?php
	// else
	// 	{
	// 		echo "<h3>Keranjang Belanja masih kosong</h3>";	
	// 	}	
?>
<!-- </form> -->


  <!-- Modal Penilai -->
  
  <!--End Modal-->
<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>