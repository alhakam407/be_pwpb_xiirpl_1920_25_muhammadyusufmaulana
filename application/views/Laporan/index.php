    <div id="content">    
        
        <div class="list1">
            <div class="right-list1">
                <?php if($this->session->userdata('ses_level')=='OWNER') :?>
                <?php else :?>
                <a href="javascript:window.history.go(-1);"> 
                    <img src="<?= base_url(); ?>assets/img/left-arrow.png"> 
                </a>
                <?php endif; ?>
                <img src="<?= base_url(); ?>assets/img/referensi.png"> 
                <h3>LAPORAN <b><span class="text-primary">MAMAM</span></b></h3>
            </div>
            <div class="right-list1">
                <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search Menu..." aria-label="Search">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
                </form> -->
            </div> 
        </div>
        
        <!-- <div class="list2-laporan">
            <button type="button" class="btn btn-success col-md-4 p-2" style="margin: 10px;">EXPORT EXCEL</button>
            <button type="button" class="btn btn-danger col-md-4 p-2" style="margin: 10px;">EXPORT PDF</button>
        </div> -->

        <div class="content-laporan">
            <div class="col-md-5 bg-white" style="margin: 0px 10px 20px 10px; border-radius: 5px; padding: 10px 0px 0px 10px">
                <p style="color: #000;"><b>LAPORAN ORDER</b></p>
                <canvas id="laporanOrder" width="300" height="300"></canvas> 
            </div>
            <div class="col-md-5 bg-white" style="margin: 0px 10px 20px 10px; border-radius: 5px; padding: 10px 0px 10px 10px">
                <p style="color: #000;"><b>LAPORAN MENU</b></p>
                <canvas id="laporanMenu" width="300" height="300"></canvas>
            </div>
        </div>

        <div class="content2-laporan">
            <div class="col-md-12" style="margin: 0px; padding: 0px; height: auto;">
                <p style="color: #000;"><b>LAPORAN HARIAN</b></p>
                <canvas id="laporanHarian" class="bg-white" style="border-radius: 5px; max-height: 80vh !important;"></canvas>
            </div>
        </div>
    </div>
        
<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

    <?php
        foreach($data as $data){
            $nama_menu[] = $data->nama_menu;
            $jumlah[] = (float) $data->jumlah;
        }
    ?>

<script src="<?= base_url(); ?>assets/charts/chart.js/dist/Chart.js"></script>
<script src="<?= base_url(); ?>assets/charts/chart.js/dist/Chart.min.js"></script>
<script>
var getChart1 = document.getElementById('laporanOrder');
var myChart1 = new Chart(getChart1, {
    type: 'bar',
    data: {
        labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni'], 
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

<!-- // ============================================================================== -->

<script>
var getChart2 = document.getElementById('laporanMenu');
var myChart2 = new Chart(getChart2, {
    type: 'pie',
    data: {
        labels: <?php echo json_encode($nama_menu);?>,
        datasets: [
            {
                data: <?php echo json_encode($jumlah);?>,
                backgroundColor: [
                    "#FF6384",
                    "#63FF84",
                    "#84FF63",
                    "#8463FF",
                    "#6384FF"
                ]
            }]
    }
});

</script>

<!-- // =============================================================================== -->

<script>
var getChart3 = document.getElementById('laporanHarian');
var dataFirst = {
    label: "Car A - Speed (mph)",
    data: [0, 59, 75, 20, 20, 55, 40],
    lineTension: 0,
    fill: false,
    borderColor: 'red'
};

var dataSecond = {
    label: "Car B - Speed (mph)",
    data: [20, 15, 60, 60, 65, 30, 70],
    lineTension: 0,
    fill: false,
    borderColor: 'blue'
};

var speedData = {
    labels: ["0s", "10s", "20s", "30s", "40s", "50s", "60s"],
    datasets: [dataFirst, dataSecond]
};

var chartOptions = {
    legend: {
        display: true,
        position: 'top',
        labels: {
            boxWidth: 80,
            fontColor: 'black'
        }
    }
};
var myChart3 = new Chart(getChart3, {
    type: 'line',
    data: speedData,
    options: chartOptions
});
</script>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>
    
<!-- modal ubah -->
    <script>
        $('.ubah').click(function () {
            $('#detailmenu').modal('hide');
        });
    </script>
    
    <!-- <script src="<?= base_url(); ?>assets/js/chart.js"></script> -->


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <!-- <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script> -->

</body>

</html>