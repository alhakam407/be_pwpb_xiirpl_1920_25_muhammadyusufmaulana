<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css">
    
    <!-- SELF CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/home/header.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/home/footer.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/home/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/aos.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/slick/slick.css"/> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/slick/slick-theme.css"/> -->

    <!-- Icon -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <title><?= $judul; ?></title> 
</head>
<body>
    
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top navku" style=" margin-bottom: 20% !important;">
            <a class="navbar-brand" href="#carouselExampleIndicators">
                <!-- <img src="assets/img/logo.png" width="50" class="d-inline-block" alt=""> -->
                <img src="assets/img/logo.png">
                MAMAM
            </a>
            <button class="navbar-toggler bg-primary" style="color: #fff !important; font-size: 30px !important;" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                ☰
            </button>

            <div class="collapse navbar-collapse putih" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto topnav">
                    <li class="nav-item">
                        <a class="nav-link navbar text-white" href="#carouselExampleIndicators">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar text-white" href="#r3">HOT PROMO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar text-white" href="#r4">MOTTO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar text-white" href="#r5">BEST SELLER</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar text-white" href="#r6">STAGE ORDERING</a>
                    </li>
                </ul>
            </div>
        </nav>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 slide" style="max-height: 650px !important; min-height: 333px !important;" src="<?= base_url();?>assets/img/slide-bg-full.jpg" alt="First slide">
            <div class="carousel-caption text-body">
                <h1 data-aos="fade-left" data-aos-duration="1500" class="text-white" style="margin-top: -40% !important; font-size: 2vw;">Butuh Yang Baru ?</h1>
                <h1 data-aos="fade-down" data-aos-duration="1500" data-aos-delay="200" class="text-white" style=" font-size: 5vw; font-weight:bold;"><span class="text-primary">MAMAM</span> DULU YUK!</h1>
                <h3 data-aos="fade-right" data-aos-duration="1500" data-aos-delay="400" class="text-white" style="font-size: 2vw;">Silahkan LOGIN Terlebih Dahulu</h3>
                <!-- <br/> -->
                <button data-aos="fade-up" data-aos-duration="2500" data-aos-delay="800" type="button" style="padding-left: 30px; padding-right: 30px; font-weight: bold;" class="btn btn-primary" data-toggle="modal" data-target="#modallogin">LOGIN</button>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 slide" style="max-height: 650px !important; min-height: 22px !important;" src="<?= base_url();?>assets/img/r3.jpg" alt="First slide">
            <div class="carousel-caption text-body">
                <!-- <h1 style="margin-top: -40% !important;"><span class="text-primary">MAMAM</span> DULU YUK!</h1> -->
                <!-- <h3>Action</h3> -->
                <!-- <button type="button" style="padding-left: 30px; padding-right: 30px;" class="btn btn-primary">LOGIN</button> -->
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100 slide" style="max-height: 650px !important; min-height: 22px !important;" src="<?= base_url();?>assets/img/r3.jpg" alt="First slide">
            <div class="carousel-caption text-body">
                <!-- <h1 style="margin-top: -40% !important;"><span class="text-primary">MAMAM</span> DULU YUK!</h1> -->
                <!-- <h3>Action</h3> -->
                <!-- <button type="button" style="padding-left: 30px; padding-right: 30px;" class="btn btn-primary">LOGIN</button> -->
            </div>
        </div>
    </div>
    <a class="carousel-control-prev bg1" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next bg1" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>