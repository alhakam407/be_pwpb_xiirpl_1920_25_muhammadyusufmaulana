<!-- CONTENT -->

        <!-- Modal -->
        <div class="modal fade" id="modallogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="exampleModalLabel">LOGIN</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-signin" action="<?php echo base_url().'home/auth'?>" method="post">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" id="email" name="email" class="form-control"
                                style="margin-bottom: 15px !important; margin-top: 15px !important;"
                                placeholder="Email / Username...">
                            <label for="pass" class="sr-only">Password</label>
                            <input type="password" id="pass" name="pass" class="form-control"
                                style="margin-bottom: 15px !important;" placeholder="Password...">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="remember-me"> Remember me 
                                </label>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Sign
                                in</button>
                            <br />
                        </form>
                        <p class="text-center">Belum Bisa MAMAM ? <a href="<?= base_url();?>Regis_umum"> Daftar!</a></p>
                    </div>
                </div>
            </div>
        </div>

        <h1 class="r1">
            <b>OUR SPECIALITIES</b>
        </h1>
        <center>
            <div class="line1-r1"></div>
            <div class="line1-r2"></div>
        </center>

        <div class="r2">
            <center>
                <div class="r2-c1">
                    <img data-aos="fade-right" data-aos-duration="1500" data-aos-delay="100" src="assets/img/r2-c1.png">
                    <h3 data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >Breakfast</h3>
                        <p data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >you need a doctor for to
                            consectetuer Lorem ipsum dolor,
                            consectetur adipiscing onsectetur Graphic.</p>
                </div>
            </center>
            <center>
                <div class="r2-c2">
                    <img data-aos="fade-right" data-aos-duration="1500" src="assets/img/r2-c2.png">
                    <h3 data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >Dessert</h3>
                        <p data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >you need a doctor for to
                            consectetuer Lorem ipsum dolor,
                            consectetur adipiscing onsectetur Graphic.</p>
                </div>
            </center>
            <center>
                <div class="r2-c3">
                    <img data-aos="fade-left" data-aos-duration="1500" src="assets/img/r2-c3.png">
                    <h3 data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >Ice Shakes</h3>
                        <p data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >you need a doctor for to
                            consectetuer Lorem ipsum dolor,
                            consectetur adipiscing onsectetur Graphic.</p>
                </div>
            </center>
            <center>
                <div class="r2-c4">
                    <img data-aos="fade-left" data-aos-duration="1500" data-aos-delay="100" src="assets/img/r2-c4.png">
                    <h3 data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >Paket</h3>
                    <p data-aos="fade-up" data-aos-duration="500" data-aos-delay="200" >you need a doctor for to
                        consectetuer Lorem ipsum dolor,
                        consectetur adipiscing onsectetur Graphic.</p>
                </div>
            </center>
        </div>


        <!-- <?php
            foreach($data as $data){
                $nama_menu[] = $data->nama_menu;
                $harga[] = $data->harga;
                $foto[] = $data->foto;
                $id_menu[] = (float) $data->id_menu;
            }
        ?> -->
        <div id="r3">
            <div class="r3-c1">
                <img src="assets/img/r3-c1.png">
            </div>
            <h2>
                <b>NEW MENU</b>
            </h2>
            <center>
                <div class="r3-c2">
                    <div class="r3-c5">
                        <img src="assets/img/r3-c2.png">
                        <h3>MAMAM Burger</br>Rp. 35.000,00</h3>
                        <a data-toggle="modal" data-target="#modallogin"><b class="text-white">ORDER NOW</b></a>
                    </div>
                    <div class="r3-c5">
                        <img src="assets/img/r3-c2.png">
                        <h3>MAMAM Burger</br>Rp. 35.000,00</h3>
                        <a data-toggle="modal" data-target="#modallogin"><b class="text-white">ORDER NOW</b></a>
                    </div>
                    <div class="r3-c5">
                        <img src="assets/img/r3-c2.png">
                        <h3>MAMAM Burger</br>Rp. 35.000,00</h3>
                        <a data-toggle="modal" data-target="#modallogin"><b class="text-white">ORDER NOW</b></a>
                    </div>
                </div>
            </center>
        </div>


        <div id="r4">
            <img src="assets/img/r4-a1.png" class="r4-a1">
            <h2>MOTTO</h2>
            <div class="r4-c1">
                <center>
                    <div class="r4-c2">
                        <img data-aos="fade-right" data-aos-duration="1000" src="assets/img/r4-c2.png">
                        <h3 data-aos="fade-right" data-aos-duration="1000">Cleanliness</h3>
                    </div>
                </center>
                <center>
                    <div class="r4-c2">
                        <img data-aos="fade-up" data-aos-duration="1000" src="assets/img/r4-c3.png">
                        <h3 data-aos="fade-up" data-aos-duration="1000">Health</h3>
                    </div>
                </center>
                <center>
                    <div class="r4-c2">
                        <img data-aos="fade-left" data-aos-duration="1000" src="assets/img/r4-c4.png">
                        <h3 data-aos="fade-left" data-aos-duration="1000">Satisfication</h3data-aos="fade-left">
                    </div>
                </center>
                <div class="r4-c3">
                    <center>
                        <blockquote>
                            <p data-aos="zoom-in" data-aos-duration="1000">" kebersihan, kesehatan dan kepuasan adalah yang utama "</p>
                            <!-- <p><span>" </span>kebersihan, kesehatan dan kepuasan adalah yang utama<span> "</span></p> -->
                        </blockquote>
                    </center>
                </div>
                <div class="r4-c4">
                    <p data-aos="flip-left" data-aos-duration="1000">BY: suufrss_</p>
                    <img src="assets/img/r4-a2.png">
                </div>
                <!-- <img src="r4-a2.png" class="r4-a2"> -->
            </div>
        </div>


        <div id="r5">
            <h2>
                <b>BEST SELLER</b>
            </h2>
            <center>
                <div class="r3-c2">
                    <div class="r3-c3">
                        <img src="assets/img/r3-c2.png">
                        <h3>MAMAM Burger</h3>
                        <a data-toggle="modal" data-target="#modallogin"><b class="text-white">ORDER NOW</b></a>
                    </div>
                    <div class="r3-c4">
                        <img src="assets/img/r3-c2.png">
                        <h3>MAMAM Burger</h3>
                        <a data-toggle="modal" data-target="#modallogin"><b class="text-white">ORDER NOW</b></a>
                    </div>
                    <div class="r3-c5">
                        <img src="assets/img/r3-c2.png">
                        <h3>MAMAM Burger</h3>
                        <a data-toggle="modal" data-target="#modallogin"><b class="text-white">ORDER NOW</b></a>
                    </div>
                </div>
                <div class="r5-c6">
                    <img src="assets/img/brush.png">
                </div>
            </center>
        </div>

        <div id="r6">
        </div>