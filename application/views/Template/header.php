<!DOCTYPE html>
<html>

<head>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"> -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/logo.png" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/dashboard.css"> -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">

    <!-- MENU -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css">
    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/datatables.css"> -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/footer.css">
    
    <!-- Transaksi -->
    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/dashboard.css"> -->

    <!-- Perusahaan -->
    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/dashboard.css"> -->

    <!-- Order -->
    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/dashboard.css"> -->

    <!-- Referensi -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/referensi.css">


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>

    <title><?= $judul; ?></title> 
</head>

<body>
    
    <div id="mySidebar" class="sidebar">
        <a href="javascript:void(0)" class="closebtn text-center" onclick="closeNav()">×</a>
        <?php if( $this->session->userdata('ses_level')=='ADMINISTRATOR') :?>
        <a href="<?= base_url(); ?>Dashboard/admin" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/dashboard.png" class="img-navbar"> DASHBOARD</a>
        <a href="<?= base_url(); ?>Menu" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/menu1.png" class="img-navbar"> MENU</a>
        <a href="<?= base_url(); ?>Transaksi" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/buy.png" class="img-navbar"> TRANSAKSI</a>
        <a href="<?= base_url(); ?>Order" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/order1.png" class="img-navbar"> ORDER</a>
        <a href="<?= base_url(); ?>Laporan" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/referensi1.png" class="img-navbar"> LAPORAN</a>
        <a href="<?= base_url(); ?>Perusahaan" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/perusahaan1.png" class="img-navbar"> PERUSAHAAN</a>
        <?php elseif( $this->session->userdata('ses_level')=='KASIR') :?>
        <a href="<?= base_url(); ?>Transaksi" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/buy.png" class="img-navbar"> TRANSAKSI</a>
        <a href="<?= base_url(); ?>Laporan" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/referensi1.png" class="img-navbar"> LAPORAN</a>
        <?php elseif( $this->session->userdata('ses_level')=='WAITER') :?>
        <a href="<?= base_url(); ?>Order" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/order1.png" class="img-navbar"> ORDER</a>
        <a href="<?= base_url(); ?>Transaksi" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/buy.png" class="img-navbar"> TRANSAKSI</a>
        <a href="<?= base_url(); ?>Laporan" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/referensi1.png" class="img-navbar"> LAPORAN</a>
        <?php elseif( $this->session->userdata('ses_level')=='PELANGGAN') :?>
        <a href="<?= base_url(); ?>Order" class="text-white navbar"><img src="<?= base_url(); ?>assets/img/order1.png" class="img-navbar"> ORDER</a>
        <?php endif;?>        
    </div>

    <div id="header">
    <?php if( $this->session->userdata('ses_level')=='ADMINISTRATOR') : ?>
        <button class="openbtn pl-4" onclick="openNav()">☰</button> 
    <?php elseif( $this->session->userdata('ses_level')=='WAITER') : ?>
        <button class="openbtn pl-4" onclick="openNav()">☰</button>
    <?php elseif( $this->session->userdata('ses_level')=='KASIR') : ?>
        <button class="openbtn pl-4" onclick="openNav()">☰</button>
    <?php endif;?>        
        <a data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
            aria-controls="multiCollapseExample1" class="profil-header" style="float: right;">
            <?php if( $this->session->userdata('ses_level')=='PELANGGAN') :?>
            <img src="<?= base_url();?>assets/img/logo.png">
            <?php else:?>
            <img src="<?= base_url(). $this->session->userdata('ses_foto');?>">
            <?php endif;?> 

            <h4><?= $this->session->userdata['ses_level']; ?></h4>
        </a>
    </div>
    <!-- PROFILE -->
    <div class="collapse multi-collapse profile shadow mb-5 bg-white rounded" id="multiCollapseExample1" style="z-index: 99999 !important;">
        <div class="card card-body" style="padding: 5px !important;">
            <div class="top-myprofile">
                <center>
                    <?php if( $this->session->userdata('ses_level')=='PELANGGAN') :?>
                    <img src="<?= base_url();?>assets/img/logo.png" style="width: 100px;">
                    <?php else:?>
                    <img src="<?= base_url(). $this->session->userdata('ses_foto');?>" style="width: 100px;">
                    <?php endif;?>
                </center>
                <p> 
                <?= $this->session->userdata('ses_nama');?>
                <br/> 
                <b><?= $this->session->userdata('ses_level');?></b>
                </p>
            </div>
            <div class="bottom-myprofile">
                <a href="<?= base_url(); ?>profile" class="btn btn-primary">Profile</a>
                <a href="<?= base_url(); ?>home/logout" class="btn btn-danger">KELUAR</a>
            </div>
        </div>
    </div> 

    

    