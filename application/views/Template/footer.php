    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>




    <script>
        function openNav() {
            document.getElementById("mySidebar").style.width = "250px";
            document.getElementById("header").style.marginLeft = "250px";
            document.getElementById("dashboard").style.marginLeft = "250px";
            document.getElementById("menu").style.marginLeft = "250px";
            document.getElementById("footer").style.marginLeft = "250px";
        }

        function closeNav() {
            document.getElementById("mySidebar").style.width = "0";
            document.getElementById("header").style.marginLeft = "0";
            document.getElementById("dashboard").style.marginLeft = "0";
            document.getElementById("menu").style.marginLeft = "0";
            document.getElementById("footer").style.marginLeft = "0";
        }
    </script>
    <!-- <script>
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script> -->
    <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="<?= base_url(); ?>assets/charts/charts.js/dist/Chart.js"></script>

</body>

</html>