    <div id="dashboard">    
        <div class="r1-d">
            <a class="r1d-l" href="<?=base_url(); ?>Menu">
                <center>
                    <img src="<?= base_url(); ?>assets/img/menu.png" class="img-content">
                    <h3 class="text-content">MENU</h3>
                </center>
            </a>
            <a class="r1d-r" href="<?=base_url(); ?>Transaksi">
                <center>
                    <img src="<?= base_url(); ?>assets/img/transaksi.png" class="img-content">
                    <h3 class="text-content">TRANSAKSI</h3>
                </center>
            </a>
        </div> 
        <div class="r2-d">
            <a class="r2d" href="<?=base_url(); ?>Perusahaan">
                <center>
                    <img src="<?= base_url(); ?>assets/img/perusahaan.png" class="img-content">
                    <h3 class="text-content">PERUSAHAAN</h3>
                </center>
            </a>
        </div>
        <div class="r3-d">
            <a class="r3d-l" href="<?=base_url(); ?>Order">
                <center>
                    <img src="<?= base_url(); ?>assets/img/order.png" class="img-content">
                    <h3 class="text-content">ORDER</h3>
                </center>
            </a>
            <a class="r3d-r" href="<?=base_url(); ?>Laporan">
                <center>
                    <img src="<?= base_url(); ?>assets/img/referensi.png" class="img-content">
                    <h3 class="text-content">LAPORAN</h3>
                </center>
            </a>
        </div>
    </div>

<!-- footer -->
    <div id="footer">
        <p style="font-weight: bold;">copyright © Aln_0197 <span style="color: #108DC5; font-weight: bold;"> MYM
                GROUP</span></p>
        <p>Version 1.1</p>
    </div>

<!-- sidebar -->
    <?php echo $scriptbuka; ?>
    <?php echo $scripttutup; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script rel="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script> -->
    <script src="<?= base_url(); ?>assets/js/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>

</html>