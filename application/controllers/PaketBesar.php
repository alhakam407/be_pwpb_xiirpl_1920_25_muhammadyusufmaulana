<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaketBesar extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_menu'); //call model
		$this->load->library('form_validation');
		$this->load->helper(array('form','url'));

		if($this->session->userdata('masuk_admin') != TRUE){
			$url=base_url();
			redirect('home');
		}
	}

	public function index() {

		$data['judul'] = 'MENU | MAMAM';
		$data['menu'] = $this->M_menu->get_paketbesar_list();
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";
		
		if( $this->input->post('keyword') ) {
		
		$data['menu'] = $this->M_menu->cariDataMenu();
		$this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/PAKET_BESAR/index', $data);

		} else{
		
		$data['status_menu'] = ['TERSEDIA', 'TIDAK TERSEDIA'];
		$this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/PAKET_BESAR/index', $data);

		}
	}

	public function tambah() {
		$data['judul'] = 'MENU | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('id_kategori', 'id_kategori', 'required');
		$this->form_validation->set_rules('nama_menu', 'nama_menu', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		$this->form_validation->set_rules('status_menu', 'status_menu', 'required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');

		if( $this->form_validation->run() == FALSE ) {
			
			$this->load->view('Template/header', $data);
			$this->load->view('ADMINISTRATOR/PAKET_BESAR/index', $data);

		} else {

			$this->M_menu->tambahDataPaketBesar();
			$this->session->set_flashdata('flash', 'Ditambah');
			redirect('paketbesar');
		}

	}

	public function ubah() {
		
		$data['judul'] = 'MENU | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('id_kategori', 'id_kategori', 'required');
		$this->form_validation->set_rules('nama_menu', 'nama_menu', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		$this->form_validation->set_rules('status_menu', 'status_menu', 'required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');

		if( $this->form_validation->run() == FALSE ) {
			
			$this->load->view('Template/header', $data);
			$this->load->view('ADMINISTRATOR/PAKET_BESAR/index', $data);

		} else {

			$this->M_menu->ubahDataPaketBesar();
			$data['nama_menu'] = $this->M_menu->getMenuById($id);
			$this->session->set_flashdata('flash', 'Diubah');
			redirect('paketbesar');
		}

	}

	public function hapus($id) {

	  	$this->M_menu->hapusDataMenu($id);
		$this->session->set_flashdata('flash', 'Dihapus');
		redirect('paketbesar');

	}
}
  