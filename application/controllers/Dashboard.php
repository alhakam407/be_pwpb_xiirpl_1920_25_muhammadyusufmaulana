<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// $this->load->model('Siswa_rpl_10');
		$this->load->library('form_validation', 'pagination');
    	$this->load->helper(array('form', 'url'));
	} 

	public function admin()
	{
		if($this->session->userdata('masuk_admin') != TRUE){
			redirect('home');
		}

		$data['judul'] = 'DASHBOARD | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

 
		$this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/index');

	}
	public function owner()
	{
		if($this->session->userdata('masuk_owner') != TRUE){
			$url=base_url();
			redirect('home');
		}
		
		$data['judul'] = 'DASHBOARD | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

 
		$this->load->view('Template/header', $data);
		$this->load->view('LAPORAN/index');
	}
	public function kasir()
	{
		if($this->session->userdata('masuk_kasir') != TRUE){
			$url=base_url();
			redirect('home');
		}
		
		$data['judul'] = 'DASHBOARD | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

 
		$this->load->view('Template/header', $data);
		$this->load->view('TRANSAKSI/list_order');
	}
	public function waiter()
	{
		if($this->session->userdata('masuk_waiter') != TRUE){
			$url=base_url();
			redirect('home');
		}
		
		$data['judul'] = 'DASHBOARD | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

 
		$this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/index');
	}
	public function pelanggan()
	{
		if($this->session->userdata('masuk_pelanggan') != TRUE){
			$url=base_url();
			redirect('home');
		}
		
		$data['judul'] = 'DASHBOARD | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

 
		$this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/index');
	}
}
 