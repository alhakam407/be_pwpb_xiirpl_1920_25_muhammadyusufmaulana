<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PERUSAHAAN extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Member');
		$this->load->library('form_validation', 'pagination');
		$this->load->helper(array('form', 'url'));
		
		if($this->session->userdata('masuk_admin') != TRUE){
			$url=base_url();
			redirect('home');
		}
    
	} 
 
	public function index() {

		$data['judul'] = 'PERUSAHAAN | MAMAM';
		$data['data1'] = $this->Member->getAllMember2();
		$data['data2'] = $this->Member->getAllMember1(); 
		$data['nama_level'] = $this->Member->getNamaLevel(); 
		
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";
		
		if( $this->input->post('keyword') ) {
		
			$data['data1'] = $this->Member->cariUser();		
			$this->load->view('Template/header', $data);
			$this->load->view('ADMINISTRATOR/PERUSAHAAN/index', $data);
		
		} else{
		
			$data['data1'] = $this->Member->getAllMember2();
			$data['data2'] = $this->Member->getAllMember1(); 
			$data['nama_level'] = $this->Member->getNamaLevel(); 
			$this->load->view('Template/header', $data);
			$this->load->view('ADMINISTRATOR/PERUSAHAAN/index', $data);
		}

    }
        
	
	public function ubah() {
		$data['data_user'] = $this->Member->getUserById($id);
		$data['nama_level'] = $this->Member->getNamaLevel(); 
		$data['judul'] = 'PERUSAHAAN | MAMAM';

		$this->form_validation->set_rules('id_level', 'id_level', 'required');

		if( $this->form_validation->run() == FALSE ) {

			// $this->load->view('template/header', $data);
			$this->load->view('Template/header', $data);
			$this->load->view('ADMINISTRATOR/PERUSAHAAN/index', $data);

		} else {
			$this->Member->ubahDataUser();
			$this->session->set_flashdata('flash', 'Diubah');
			redirect('perusahaan');
		}
			
	} 

	public function hapus($id) {

	  	$this->Member->hapusDataUser($id);
		$this->session->set_flashdata('flash', 'Dihapus');
		redirect('perusahaan');

	}

	public function datauser() {
		$data['judul'] = 'PERUSAHAAN | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

        if($this->input->post('keyword')) { 

		$data['data'] = $this->Member->cariUser();
				
		
        $this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/PERUSAHAAN/list_user', $data);
          // $this->load->view('template/footer');

        } else {

        //konfigurasi pagination
        $config['base_url'] = site_url('perusahaan/datauser'); //site url
        $config['total_rows'] = $this->db->count_all('v_user'); //total row
        $config['per_page'] = 5;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>'; 
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
        $data['data'] = $this->Member->get_user_list($config["per_page"], $data['page']);           
		$data['pagination'] = $this->pagination->create_links();
		

        $this->load->view('Template/header', $data);
		$this->load->view('ADMINISTRATOR/PERUSAHAAN/list_user', $data);
        }
	}
}
 