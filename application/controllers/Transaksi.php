<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TRANSAKSI extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('m_menu','transaksi_model','order_model')); //call model
		$this->load->library('form_validation');
		$this->load->helper(array('form','url'));

		if($this->session->userdata('ses_level') == 'ADMINISTRATOR'){

		}elseif($this->session->userdata('ses_level') == 'KASIR'){
			
		}elseif($this->session->userdata('ses_level')=='WAITER'){

		}else{
			redirect('home'); 
		}
	}

	public function index()
	{
		
		$data['judul'] = 'LIST TRANSAKSI | MAMAM'; 
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('history','history','required');
        if($this->form_validation->run()== false) {  

			$data['data1'] = $this->transaksi_model->get_transaksi_list();   
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/list_transaksi', $data);

		} else {

        	$this->transaksi_model->softDeleteTransaksi();
			redirect('transaksi');

		}
	}
	
	public function list_orderKASIR(){
		$data['judul'] = 'LIST ORDER | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('history','history','required');

		if($this->form_validation->run()== false){

			$data['data1'] = $this->transaksi_model->get_order_list_kasir();   
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/K_list_order', $data);
		} else{
			$this->transaksi_model->softDeleteOrder();
			redirect('transaksi/list_orderKASIR');
		}
	}

	public function list_orderWAITER(){
		$data['judul'] = 'LIST ORDER | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$data['data1'] = $this->transaksi_model->get_order_list_waiter(); 
		$data['data2'] = $this->transaksi_model->statusDetailOrder();
        $this->load->view('Template/header', $data);
		$this->load->view('TRANSAKSI/w_list_order', $data);
	}

	public function inserttransaksi($id){
		$data['judul'] = ' TRANSAKSI | INSERT';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('dibayar','dibayar','required');

		if( $this->form_validation->run()== false){

			$data['data3'] = $this->transaksi_model->insert_transaksi($id);
	
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/insert_transaksi', $data);
		}else{
			$this->transaksi_model->inputTransaksi();
			redirect('transaksi');
		}
	}

	public function edittransaksiKASIR($id){
		$data['judul'] = ' TRANSAKSI | EDIT';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('dibayar','dibayar','required');

		if( $this->form_validation->run()== false){

			$data['status'] = ['DALAM PROSES', 'SELESAI'];
			$data['data3'] = $this->transaksi_model->edit_transaksi($id);
	
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/k_edit_transaksi', $data);
		}else{
			$this->transaksi_model->editTransaksi();
			redirect('transaksi');
		}
	}

	public function edittransaksiWAITER($id){
		$data['judul'] = ' TRANSAKSI | EDIT';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		} 
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('status_pesanan','status_pesanan','required');

		if( $this->form_validation->run()== false){

			$data['status'] = ['DALAM PROSES', 'SELESAI'];
			$data['data3'] = $this->transaksi_model->edit_transaksi($id);
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/w_edit_transaksi', $data);

		}else{

			$this->order_model->editStatusPesanan();
			redirect('transaksi');
		}
	}

	public function detailListOrder($id){
		$data['judul'] = ' TRANSAKSI | DETAIL';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";


			$data['status'] = ['DALAM PROSES', 'SELESAI'];
			$data['data3'] = $this->transaksi_model->detail_order($id);
			// $data['data4'] = $this->order_model->getOrderById($id);
	
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/detail_transaksi', $data);
	}

	public function detailtransaksi($id){
		$data['judul'] = ' TRANSAKSI | DETAIL';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";


			$data['status'] = ['DALAM PROSES', 'SELESAI'];
			$data['data3'] = $this->transaksi_model->detail_transaksi($id);
			// $data['data3'] = $this->transaksi_model->edit_transaksi($id);
			// $data['data4'] = $this->order_model->getOrderById($id);
	
			$this->load->view('Template/header', $data);
			$this->load->view('TRANSAKSI/detail_transaksi', $data);
	}
    
}
 