<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	private $db2;
	private $db3;
	private $db4; 
	private $db5;
	private $db6;
	
	function __construct(){
		parent::__construct();
		$this->load->model(array('Account_user','m_menu'));
		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->db2 = $this->load->database('admin', TRUE);
		$this->db3 = $this->load->database('own', TRUE);
		$this->db4 = $this->load->database('kasir', TRUE);
		$this->db5 = $this->load->database('wtr', TRUE);
		$this->db6 = $this->load->database('plg', TRUE);
	}

	public function index()
	{
		$data['judul'] = 'HOME | MAMAM'; 
		$data['data'] = $this->m_menu->getNewMenu();

		$this->load->view('Home/header', $data);
		$this->load->view('Home/index', $data); 
		$this->load->view('Home/footer');
	}

	public function auth() {

		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$where = array(
			'email' => $email,
			'pass' => $pass,
		);
		//var_dump($pass);die();

		$administrator = $this->db2->get_where('tb_user', ['email' => $email])->row_array();
		$owner = $this->db3->get_where('tb_user', ['email' => $email])->row_array();
		$kasir = $this->db4->get_where('tb_user', ['email' => $email])->row_array();
		$waiter = $this->db5->get_where('tb_user', ['email' => $email])->row_array();
		$pelanggan = $this->db6->get_where('tb_user', ['email' => $email])->row_array();

			if($administrator){
				// membaca akses admin; 
				if(password_verify($pass,$administrator['pass'])){
					$data = [
						'email' => $administrator['email'],
						// 'foto' => $administrator['foto'],
						'id_level' => $administrator['id_level']
					];
					$this->session->set_userdata($data);
					// redirect('dashboard');
					if( $administrator['id_level'] == 1){
						$this->session->set_userdata('masuk_admin',TRUE); 
						$this->session->set_userdata('ses_id',$administrator['id']); 
						$this->session->set_userdata('ses_foto',$administrator['foto']); 
						$this->session->set_userdata('ses_nama',$administrator['nama']); 
						$this->session->set_userdata('ses_email',$administrator['email']); 
						// $this->session->set_userdata('ses_pass',$administrator['pass']); 
						$this->session->set_userdata('ses_level',"ADMINISTRATOR");  
						redirect('dashboard/admin');
					}elseif($owner){
						// membaca akses owner; 
						if(password_verify($pass,$owner['pass'])){
							$data = [
								'email' => $owner['email'],
								'id_level' => $owner['id_level']
							];
							$this->session->set_userdata($data);

							if( $owner['id_level'] == 2){
								$this->session->set_userdata('masuk_owner',TRUE);
								$this->session->set_userdata('ses_id',$owner['id']); 
								$this->session->set_userdata('ses_foto', $owner['foto']);
								$this->session->set_userdata('ses_nama',$owner['nama']);
								$this->session->set_userdata('ses_email',$owner['email']);
								$this->session->set_userdata('ses_level',"OWNER"); 
								// $this->session->set_userdata('ses_nama',$owner['nama']);
								redirect('dashboard/owner');
								// echo"test";die();
							}elseif($kasir){
								// membaca hak akses kasir  
								if(password_verify($pass,$kasir['pass'])){
									$data = [
										'email' => $kasir['email'],
										'id_level' => $kasir['id_level']
									];
									$this->session->set_userdata($data);
									if( $kasir['id_level'] == 3){
										$this->session->set_userdata('masuk_kasir',TRUE);
										$this->session->set_userdata('ses_id',$kasir['id']); 
										$this->session->set_userdata('ses_foto', $owner['foto']);
										$this->session->set_userdata('ses_nama',$owner['nama']);
										$this->session->set_userdata('ses_email',$owner['email']);
										$this->session->set_userdata('ses_level',"KASIR"); 	
										redirect('transaksi');
									}elseif($waiter){
										// membaca akses waiter
										if(password_verify($pass,$waiter['pass'])){
											$data = [
												'email' => $waiter['email'],
												'id_level' => $waiter['id_level']
											];
											$this->session->set_userdata($data);
											if( $waiter['id_level'] == 4){
												$this->session->set_userdata('masuk_waiter',TRUE);
												$this->session->set_userdata('ses_id',$waiter['id']); 
												$this->session->set_userdata('ses_foto', $owner['foto']);
												$this->session->set_userdata('ses_nama',$owner['nama']);
												$this->session->set_userdata('ses_email',$owner['email']);
												$this->session->set_userdata('ses_level',"WAITER"); 
												redirect('order');
											}elseif($pelanggan){
												// membaca akses pelanggan
												if(password_verify($pass,$pelanggan['pass'])){
													$data = [
														'email' => $pelanggan['email'],
														'id_level' => $pelanggan['id_level']
													];
													$this->session->set_userdata($data);
													if( $pelanggan['id_level'] == 5){
														$this->session->set_userdata('masuk_pelanggan',TRUE);
														$this->session->set_userdata('ses_id',$pelanggan['id']); 
														$this->session->set_userdata('ses_foto', base_url()."assets/img/logo.png");
														$this->session->set_userdata('ses_nama',$owner['nama']);
														$this->session->set_userdata('ses_email',$owner['email']);
														$this->session->set_userdata('ses_level',"PELANGGAN"); 
														redirect('order');
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}else{
						// echo"username password tidak ditemukan!";
						redirect('home');
					} 
			}
	}
	function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect('home');
    }
		// var_dump($email);die();
		// var_dump($pass);die();
}
