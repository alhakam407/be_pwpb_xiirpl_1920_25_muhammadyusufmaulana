<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PROFILE extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('Account_user','transaksi_model')); 
		$this->load->library('form_validation');
		$this->load->helper(array('form','url'));

		if($this->session->userdata('ses_level') == 'ADMINISTRATOR'){

		}elseif($this->session->userdata('ses_level') == 'OWNER'){
			
		}elseif($this->session->userdata('ses_level') == 'KASIR'){
			
		}elseif($this->session->userdata('ses_level') == 'WAITER'){
			
		}elseif($this->session->userdata('ses_level') == 'PELANGGAN'){
			
		}else{
			redirect('home');
		}
	}

	public function index()
	{
		$data['judul'] = 'PROFILE | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->load->view('Template/header', $data);
		$this->load->view('PROFIL/index', $data);
		// $this->load->view('Template/footer');
    }
    
    public function edit()
	{
		$data['judul'] = 'PROFILE | EDIT';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>"; 
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$this->form_validation->set_rules('pass','pass', 'required');

		if( $this->session->userdata('ses_level')=='ADMINISTRATOR'){
			if( $this->form_validation->run()== FALSE){
				$this->load->view('Template/header', $data);
				$this->load->view('PROFIL/edit', $data);	
			} else{
				$this->Account_user->ubahProfileAdmin();
				// $data['nama_menu'] = $this->M_menu->getMenuById($id);
				$this->session->set_flashdata('flash', 'Diubah');
				$this->session->sess_destroy();
				$url=base_url('');
				redirect('home');
			} 
		} elseif( $this->session->userdata('ses_level')=='OWNER'){
			if( $this->form_validation->run()== FALSE){
				$this->load->view('Template/header', $data);
				$this->load->view('PROFIL/edit', $data);	
			} else{
				$this->Account_user->ubahProfileOwner();
				// $data['nama_menu'] = $this->M_menu->getMenuById($id);
				$this->session->set_flashdata('flash', 'Diubah');
				redirect('profile');
			}
		} elseif( $this->session->userdata('ses_level')=='KASIR'){
			if( $this->form_validation->run()== FALSE){
				$this->load->view('Template/header', $data);
				$this->load->view('PROFIL/edit', $data);	
			} else{
				$this->Account_user->ubahProfileKasir();
				// $data['nama_menu'] = $this->M_menu->getMenuById($id);
				$this->session->set_flashdata('flash', 'Diubah');
				redirect('profile');
			}
		} elseif( $this->session->userdata('ses_level')=='WAITER'){
			if( $this->form_validation->run()== FALSE){
				$this->load->view('Template/header', $data);
				$this->load->view('PROFIL/edit', $data);	
			} else{
				$this->Account_user->ubahProfileWaiter();
				// $data['nama_menu'] = $this->M_menu->getMenuById($id);
				$this->session->set_flashdata('flash', 'Diubah');
				redirect('profile');
			}
		} elseif( $this->session->userdata('ses_level')=='PELANGGAN'){
			if( $this->form_validation->run()== FALSE){
				$this->load->view('Template/header', $data);
				$this->load->view('PROFIL/edit', $data);	
			} else{
				$this->Account_user->ubahProfilePelanggan();
				// $data['nama_menu'] = $this->M_menu->getMenuById($id);
				$this->session->set_flashdata('flash', 'Diubah');
				redirect('profile');
			}
		}

    }
}
