<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('laporan_model'); 
		// $this->load->library('form_validation', 'pagination');
    	// $this->load->helper(array('form', 'url'));
    
		if($this->session->userdata('ses_level')== 'ADMINISTRATOR'){

		}elseif($this->session->userdata('ses_level')== 'KASIR'){

		}elseif($this->session->userdata('ses_level')== 'WAITER'){

		}elseif($this->session->userdata('ses_level')== 'OWNER'){

		}else{ 
			$url=base_url();
			redirect('home');
		} 
	}
 
	public function index()
	{ 
		$data['judul'] = 'MENU | MAMAM';
		$data["scriptbuka"] = "<script> function openNav() {
			document.getElementById('mySidebar').style.width = '250px';
		}
		</script>";
		$data["scripttutup"] = "<script> function closeNav() {
			document.getElementById('mySidebar').style.width = '0';
		}
		</script>";

		$data['data'] = $this->laporan_model->getLaporanMenu();

		$this->load->view('Template/header', $data);
		$this->load->view('Laporan/index', $data);
	}
}
