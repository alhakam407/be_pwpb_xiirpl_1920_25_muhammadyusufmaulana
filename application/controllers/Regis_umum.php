<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class REGIS_UMUM extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Account_user'); //call model
		$this->load->library('form_validation');
		$this->load->helper(array('form','url'));

	}

	public function index() {
		
		$data['judul'] = "Regis | MAMAM";

		$this->form_validation->set_rules('nama', 'nama','required');
		$this->form_validation->set_rules('email', 'email','required|is_unique[tb_user.email]');
		$this->form_validation->set_rules('pass','pass','required');
		$this->form_validation->set_rules('pass_conf','pass_conf','required|matches[pass]');
		$this->form_validation->set_rules('id_level', 'id_level','required');

		if( $this->form_validation->run() == FALSE) {
			$this->load->view('Regis/form_umum', $data);
		} else{ 
			
			$this->Account_user->daftarUser();
			$this->session->set_flashdata('flash', 'Ditambah'); 
			redirect('home');
		}
	}
}

 