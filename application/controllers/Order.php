<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		$this->load->library('cart');
		$this->load->model(array('order_model','m_menu','transaksi_model'));

		if($this->session->userdata('ses_level') == 'ADMINISTRATOR'){

		}elseif($this->session->userdata('ses_level') == 'WAITER'){
			
		}elseif($this->session->userdata('ses_level')== 'PELANGGAN'){

		}
		else{
			redirect('home');
		}
	}

	public function index()
	{
        $data['judul'] = 'ORDER | MAMAM';
        $data["scriptbuka"] = "<script> function openNav() {
            document.getElementById('mySidebar').style.width = '250px';
        } 
        </script>";
        $data["scripttutup"] = "<script> function closeNav() {
            document.getElementById('mySidebar').style.width = '0';
        }
        </script>";

		$kategori=($this->uri->segment(3))?$this->uri->segment(3):0;
		$data['menu'] = $this->order_model->get_menu_kategori($kategori);
		$data['kategori'] = $this->order_model->get_kategori_all(); 
		$data['status_menu'] = ['TERSEDIA', 'TIDAK TERSEDIA'];
		$this->load->view('Template/header',$data);
		$this->load->view('ORDER/index',$data);
		// $this->load->view('themes/footer');
	}
	public function tampil_cart() 
	{
		$data['judul'] = 'ORDER | KERANJANG';
        $data["scriptbuka"] = "<script> function openNav() {
            document.getElementById('mySidebar').style.width = '250px';
        }
        </script>";
        $data["scripttutup"] = "<script> function closeNav() {
            document.getElementById('mySidebar').style.width = '0';
        }
        </script>";
		$data['kategori'] = $this->order_model->get_kategori_all(); 
		$this->load->view('Template/header',$data);
		$this->load->view('ORDER/keranjang',$data);
		// $this->load->view('themes/footer');
	}
	
	public function check_out()
	{
		$data['judul'] = 'ORDER | CHECK OUT';
        $data["scriptbuka"] = "<script> function openNav() {
            document.getElementById('mySidebar').style.width = '250px';
        }
        </script>";
        $data["scripttutup"] = "<script> function closeNav() {
            document.getElementById('mySidebar').style.width = '0';
        }
        </script>";
		$data['kategori'] = $this->order_model->get_kategori_all();
		$this->load->view('Template/header',$data);
		$this->load->view('ORDER/check_out',$data);
		// $this->load->view('themes/footer');
	}
	
	function tambah()
	{
		
		$data_menu= array(
            'id' => $this->input->post('id'),	
			'foto' => $this->input->post('foto'),
			'name' => $this->input->post('nama_menu'),
			'price' => $this->input->post('harga'),
			'qty' => $this->input->post('jumlah')
		);
        
        $this->cart->insert($data_menu);
		redirect('order');
	}

	function hapus($rowid) 
	{
		if ($rowid=="all")
			{
				$this->cart->destroy();
			}
		else
			{
				$data = array('rowid' => $rowid,
			  				  'qty' =>0);
				$this->cart->update($data);
			}
		redirect('order/tampil_cart');
	}

	function ubah_cart()
	{
		$cart_info = $_POST['cart'] ;
		foreach( $cart_info as $id => $cart)
		{
			$rowid = $cart['rowid'];
			$price = $cart['price'];
			$name = $cart['name'];
			$foto = $cart['foto'];
			$amount = $harga * $cart['jumlah'];
			$qty = $cart['qty'];
			$data = array('rowid' => $rowid,
							'price' => $price,
							'name' => $name,
							'foto' => $foto,
							'amount' => $amount,
							'qty' => $qty);
			$this->cart->update($data);
		}
		redirect('order/tampil_cart');
	}

	public function proses_order()
	{
		//-------------------------Input data pelanggan-------------------------- 		
		$this->form_validation->set_rules('nama_pelanggan', 'nama_pelanggan', 'required');
		
		if ($this->session->userdata('ses_level')=='ADMINISTRATOR'){
			if ( $this->form_validation->run() == false) {
				$data['judul'] = 'MENU | MAMAM';
				$data["scriptbuka"] = "<script> function openNav() {
					document.getElementById('mySidebar').style.width = '250px';
				}
				</script>";
				$data["scripttutup"] = "<script> function closeNav() {
					document.getElementById('mySidebar').style.width = '0';
				}
				</script>";

				$this->load->view('Template/header', $data);
				$this->load->view('ORDER/check_out', $data);
			} else{
				
				$id_user = $this->input->post('id_user');
				$nama_pelanggan = $this->input->post('nama_pelanggan');
				$no_meja = $this->input->post('no_meja');
				$waktu_order = $this->input->post('waktu_order');
				$catatan = $this->input->post('catatan');
				$status_order = $this->input->post('status_order');
				$id_menu = $this->input->post('id_menu');
				$jumlah = $this->input->post('jumlah');
				$sub_total = $this->input->post('sub_total'); 
				$status_detail_order = $this->input->post('status_detail_order');
				$status_pesanan = $this->input->post('status_pesanan');
				$history = $this->input->post('history');
				$grand_total = $this->input->post('total_bayar');

				$dataorder = array(
					'id_user' => $id_user,
					'nama_pelanggan' => $nama_pelanggan,
					'no_meja' => $no_meja,
					'waktu_order' => date('d/m/Y'),
					'catatan' => $catatan,
					'status_pesanan' => $status_pesanan,
					'history' => $history,
					'status_order' => $status_order
				);
				$id = $this->order_model->inputorderadmin($dataorder, 'tb_order');

				if ($cart = $this->cart->contents())
	            {
        	        foreach ($cart as $item)
                    {
                        $datadetailorder = array('id_order' =>$id,
                                        'id_menu' => $item['id'],
                                        'jumlah' => $item['qty'],
										'sub_total' => $item['subtotal'],
										'total_bayar' => $grand_total,
										'status_detail_order' => $status_detail_order
									);
                        $this->order_model->inputdetailorderadmin($datadetailorder, 'tb_detail_order');
                    }
				}

				$this->cart->destroy();
				redirect('order');
			}
		} elseif( $this->session->userdata('ses_level')=='WAITER'){
			if ( $this->form_validation->run() == false) {
				$data['judul'] = 'MENU | MAMAM';
				$data["scriptbuka"] = "<script> function openNav() {
					document.getElementById('mySidebar').style.width = '250px';
				}
				</script>";
				$data["scripttutup"] = "<script> function closeNav() {
					document.getElementById('mySidebar').style.width = '0';
				}
				</script>";

				$this->load->view('Template/header', $data);
				$this->load->view('ORDER/check_out', $data);
			} else{
				
				$id_user = $this->input->post('id_user');
				$nama_pelanggan = $this->input->post('nama_pelanggan');
				$no_meja = $this->input->post('no_meja');
				$waktu_order = $this->input->post('waktu_order');
				$catatan = $this->input->post('catatan');
				$status_order = $this->input->post('status_order');
				$id_menu = $this->input->post('id_menu');
				$jumlah = $this->input->post('jumlah');
				$sub_total = $this->input->post('sub_total');
				$status_detail_order = $this->input->post('status_detail_order');
				$grand_total = $this->input->post('total_bayar');

				$dataorder = array(
					'id_user' => $id_user,
					'nama_pelanggan' => $nama_pelanggan,
					'no_meja' => $no_meja, 
					'waktu_order' => date('d/m/Y'),
					'catatan' => $catatan,
					'status_order' => $status_order 
				);
				$id = $this->order_model->inputorderwaiter($dataorder, 'tb_order');

				if ($cart = $this->cart->contents())
	            {
        	        foreach ($cart as $item)
                    {
                        $datadetailorder = array('id_order' =>$id,
                                        'id_menu' => $item['id'],
                                        'jumlah' => $item['qty'],
										'sub_total' => $item['subtotal'],
										'total_bayar' => $grand_total,
										'status_detail_order' => $status_detail_order
									);
                        $this->order_model->inputdetailorderwaiter($datadetailorder, 'tb_detail_order');
                    }
    	        }

				$this->cart->destroy();
				redirect('order');
			}
		} elseif( $this->session->userdata('ses_level')=='PELANGGAN'){
			if ( $this->form_validation->run() == false) {
				$data['judul'] = 'MENU | MAMAM';
				$data["scriptbuka"] = "<script> function openNav() {
					document.getElementById('mySidebar').style.width = '250px';
				}
				</script>";
				$data["scripttutup"] = "<script> function closeNav() {
					document.getElementById('mySidebar').style.width = '0';
				}
				</script>";

				$this->load->view('Template/header', $data);
				$this->load->view('ORDER/check_out', $data);
			} else{
				
				$id_user = $this->input->post('id_user');
				$nama_pelanggan = $this->input->post('nama_pelanggan');
				$no_meja = $this->input->post('no_meja');
				$waktu_order = $this->input->post('waktu_order');
				$catatan = $this->input->post('catatan');
				$status_order = $this->input->post('status_order');
				$id_menu = $this->input->post('id_menu');
				$jumlah = $this->input->post('jumlah');
				$sub_total = $this->input->post('sub_total');
				$status_detail_order = $this->input->post('status_detail_order');
				$grand_total = $this->input->post('total_bayar');

				$dataorder = array(
					'id_user' => $id_user,
					'nama_pelanggan' => $nama_pelanggan,
					'no_meja' => $no_meja,
					'waktu_order' => date('d/m/Y'),
					'catatan' => $catatan,
					'status_order' => $status_order
				);
				$id = $this->order_model->inputorderpelanggan($dataorder, 'tb_order');

				if ($cart = $this->cart->contents())
	            {
        	        foreach ($cart as $item)
                    {
                        $datadetailorder = array('id_order' =>$id,
                                        'id_menu' => $item['id'],
                                        'jumlah' => $item['qty'],
										'sub_total' => $item['subtotal'],
										'total_bayar' => $grand_total,
										'status_detail_order' => $status_detail_order
									);
                        $this->order_model->inputdetailorderpelanggan($datadetailorder, 'tb_detail_order');
                    }
    	        }

				$this->cart->destroy();
				redirect('order');
			}
		}
		
	}

	public function hapusorder($id){
		
		$this->transaksi_model->hapusDataOrder($id);
		$this->session->set_flashdata('flash', 'Dihapus');

		redirect('transaksi/list_order');
	}
}
?>