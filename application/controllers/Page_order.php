
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_order extends CI_Controller {
	public function __construct()
	{	
		parent::__construct();
		$this->load->library('cart');
		$this->load->model(array('order_model', 'm_menu'));
	}
	public function index()
		{
            $data['judul'] = 'MENU | MAMAM';
            $data["scriptbuka"] = "<script> function openNav() {
                document.getElementById('mySidebar').style.width = '250px';
            }
            </script>";
            $data["scripttutup"] = "<script> function closeNav() {
                document.getElementById('mySidebar').style.width = '0';
            }
            </script>";
            if( $this->input->post('keyword') ){
                $data['menu'] = $this->M_menu->cariDataMenu();
    	    	$this->load->view('Template/header', $data);
	    	    $this->load->view('ORDER/index', $data);
            }else{

                $data['menu'] = $this->order_model->get_menu_all();
                $data['kategori'] = $this->order_model->get_kategori_all();
                $this->load->view('Template/header',$data); 
                $this->load->view('ORDER/index',$data);
                // $this->load->view('themes/footer');
            }
		}
}
